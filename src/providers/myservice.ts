import { Injectable } from '@angular/core';
import { Http,RequestOptions ,Headers} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { LoadingController, AlertController, Events   } from 'ionic-angular';


@Injectable()
export class Myservice {
  webservice_url = 'http://techlabz.in/Reshma/Iveride/Web_service/';
  loader : any;
  alert : any;
  post_url : any;
  data : any;
  result:any;
  get_url:any;
  
  constructor(public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public events: Events ) {
  }
  
    show_loader() {
  this.loader = this.loadingCtrl.create({
      spinner: 'hide',
      content:"<img src='assets/img/jr_loader.gif'>"
    });
  this.loader.present();
  }
  
  show_alert(title, message) {
      this.alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    this.alert.present();
  }
  
  hide_loader() {
    var This=this;
     This.loader.dismissAll();
  }
  /*load_new() {
    this.http.get('https://randomuser.me/api/?results=10')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        console.log(this.data);
      });
  }*/

  load_post(post_data, fn_name) {
      
    
    post_data = JSON.stringify(post_data);
    this.post_url = this.webservice_url + fn_name;
      let headers = new Headers();
      headers.append('X_API_KEY','');
      let options = new RequestOptions({ headers });
     return this.http.post(`${this.post_url}`, `${post_data}`, options) .map(res => res.json());
   
  
  }

  obj_array(data){
    var res = new Array();
    for(var i in data){
      res.push(data[i]);
    }
    return res;
  }
}
