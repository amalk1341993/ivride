import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth, AngularFireAuthProvider} from 'angularfire2/auth';
import { LoadingController, AlertController, Events   } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FirebaseService {

  items: FirebaseListObservable<any[]>;
  //data: FirebaseObjectObservable<any[]>;
 data:any;
  currentUser: any;
  userData: any;

  private basePath: string = 'users';
    alert : any;
    loader : any;
  constructor(
                private db: AngularFireDatabase,
                public afAuth: AngularFireAuth,) {
                   this.currentUser = this.afAuth.auth.currentUser;
              console.log(this.currentUser);
              //       console.log('hhhh');
               }


  /** ================================================================================== 
      ================================================================================== */
  pushItem(url,data){
     console.log(url);
     console.log(data);
     return this.db.list(url).push(data).key;
  }
  createItem(url,data){
     return this.db.object(url).set(data);
  }
  
  updateItem(url,data){
   // alert('kk');
     return this.db.object(url).update(data);
  }
  removeItem(url){
     return this.db.object(url).remove();
  }
  
  listItem(url){
    return  this.db.list(url);
      
  }
  listItemQuery(url,query){
    return  this.db.list(url,query);
      
  }
    registerUser(id: any, item: any){
    return this.db.object("users/" + id).set(item);
  }

  setItem(url:any,id: any, item: any){
    return this.db.object(url+"/"+ id).set(item);
  }
  getCurrentUser(){
    return this.afAuth.auth.currentUser;
  }
  getUser(id: string){
    return this.db.list("users/" + id);
  }
  
    updateUserData(key: string, data:any) {
  
    const itemPath =  `${this.basePath}/${key}`;
    return this.db.object(itemPath).update(data);
  
  }

   checkItem(url){
    return this.db.object(url);
  }
    listrecentItem(url){
    return  this.db.list(url);
      
  }
}

