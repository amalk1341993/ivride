import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Yourride } from './yourride';

@NgModule({
  declarations: [
    Yourride,
  ],
  imports: [
    IonicPageModule.forChild(Yourride),
  ],
  exports: [
    Yourride
  ]
})
export class YourrideModule {}
