import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';

@IonicPage()
@Component({
  selector: 'page-yourride',
  templateUrl: 'yourride.html'
})
export class Yourride {
tab:any;
userid:any;
offerdata:any;
data:any;
book_data:any;
offercount:any;
offerdatalength:any;
rideshowbtn:any;
bookcount:any;
bookshowbtn:any;
bookdatalength:any;
offerhistory:any;
book_history:any;
source_sp:any;
source_head:any;
source_new:any;
destination_sp:any;
destination_head:any;
destination_new:any;
ride_time:any;
ampm:any;
bookhistshowbtn:any;
formatted_time:any;
reached_time:any;
formatted_reached_time:any;
bookhistcount:any;
offercounthistory:any;
offerhistcount:any;
ridehistshowbtn:any;
 rootdb:any;
 user_id:any;
 result:any;
 currentUser: any;
 date:any;
 current_offers:any;
 history_offers:any;
 
 //currentUser: any;
  constructor( public navCtrl: NavController,
               private myservice: Myservice, 
               public storage: Storage, 
               public viewCtrl: ViewController,
               private toastCtrl: ToastController, 
               public navParams: NavParams,
               public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public popoverCtrl: PopoverController,
          ) {
     /*var users = this.firebaseService.getCurrentUser();
     if(users){
   
      setTimeout(() =>{
       this.userid = users.uid;
       this.booking_count();
       this.offerdetails_count();
       this.offer_ride_details();
       this.book_ride_details();
       this.ride_rate_user();
       this.history_offerdata();
       this.history_booking();
       this.booking_history_count();
       this.offerdetails_history_count();
       	},1000);

   }
    
      this.rideshowbtn=true;
      this.bookshowbtn=true;
      this.bookhistshowbtn=true;
      this.ridehistshowbtn=true;*/

      this.rootdb  = firebase.database().ref();
       this.get_offered();
     


  }


  get_offered(){
    var This = this;
    var users = this.firebaseService.getCurrentUser();
    console.log(users.uid);
    this.current_offers = new Array();
    this.history_offers = new Array();
    this.rootdb.child('booking').orderByChild('user_id').equalTo(users.uid).once('value',function(snap){
      var data = snap.val();
      console.log(data);

      var curr = This.check_date();
      This.result = This.myservice.obj_array(data);
      This.result.forEach((item,index) => {

        This.rootdb.child('car_details').child(item.car_id).once('value',function(data){
          item.car = data.val();
        })



        if(curr>=item.departure_date){
          This.current_offers.push(item);
        } else {
          This.history_offers.push(item);
        }
      })
      console.log(This.current_offers);
      console.log(This.history_offers);
    })
  }


  get_loc(loc){
    var y = loc.split(",");
    return y[0];
  }


  check_date(){
      this.date=new Date();
      var dd = this.date.getDate();
      var mm = this.date.getMonth()+1; //January is 0!
      var yyyy = this.date.getFullYear();

      if(dd<10) {
          dd = '0'+dd
      } 
      if(mm<10) {
          mm = '0'+mm
      } 
      return yyyy + '-' + mm + '-' + dd;
      
  }

  map_page(data){
    //this.navCtrl.push('Locations',{'data':data});
    console.log(data);
    this.navCtrl.push('Drivermap',{'data':data});
  }
  history_booking(){
/*     console.log(this.userid);
         var bookdata={'user_id':this.userid,
                     'start':''

      }
      // this.myservice.show_loader();
      this.myservice.load_post(bookdata,"booking_ride_history").subscribe(response => {
	  	// this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {

         
            this.book_history=response.data;
            this.book_history.forEach((element,j )=> {
               this.get_source_book_split(element,j);
   
               console.log(this.source_sp[0]);
               this.book_history[j].source_head= this.source_head;
               this.book_history[j].source_new= this.source_new;
               this.book_history[j].destination_head=  this.destination_head;
               this.book_history[j].destination_new=  this.destination_new;
               this.book_history[j].detour_time=this.formatted_time
               console.log( this.book_data);
                
            });
          }
       
      })*/

  }
  history_offerdata(){
    var users = this.firebaseService.getCurrentUser();
       var This = this;
       This.rootdb  = firebase.database().ref(); 
       This.rootdb.child('ride').on('value',function(snap){
           This.book_history = snap.val(); //console.log(This.usercar);
              if(This.book_history!=null && This.book_history!='' ){
                   var new_array = new Array();
                  for(var i in This.book_history){
                    if(This.book_history[i].users_id==users.uid){
                    new_array.push(This.book_history[i]);
                   }
                  }
                  This.book_history = new_array;console.log(This.book_history);
            }
          })



    	
/*     var offerdata={'user_id':this.userid,
                     'start':''
                     

      }
      // this.myservice.show_loader();
          this.myservice.load_post(offerdata,"offer_history_ride").subscribe(response => {
	  	// this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
          this.offerhistory=response.data;
            console.log(response.data);
                 this.offerhistory.forEach((element,j )=> {
               this.get_source_split(element,j);

          this.offerhistory[j].source_head= this.source_head;
           this.offerhistory[j].source_new= this.source_new;
           this.offerhistory[j].destination_head=  this.destination_head;
           this.offerhistory[j].destination_new=  this.destination_new;
           this.offerhistory[j].detour_time=this.formatted_time;
            console.log(this.offerhistory);
             
           });

          }
       
      })*/

  }

  offerdetails_count(){
    	// this.myservice.show_loader();
     var This = this;
       This.rootdb  = firebase.database().ref(); 
       This.rootdb.child('ride').on('value',function(snap){
           This.offercount = snap.val(); //console.log(This.usercar);
              if(This.offercount!=null && This.offercount!='' ){
                   var new_array = new Array();
                  for(var i in This.offercount){
                    if(This.offercount[i].user_id==This.userid){
                    new_array.push(This.offercount[i]);
                   }
                  }
                  This.offercount = new_array;console.log(This.book_history);
               if(This.offercount>5){
              This.rideshowbtn=false;
            }
            }
          })









      /*this.myservice.load_post(this.userid,"offer_ride_count_details").subscribe(response => {
	  	// this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
         
            console.log(response.data);
            this.offercount=response.data.totalcount;
            if(this.offercount>5){
              this.rideshowbtn=false;
              

            }
          }
       
      })*/

  }
    offerdetails_history_count(){
    
      // this.myservice.show_loader();
     /* this.myservice.load_post(this.userid,"offer_history_count_details").subscribe(response => {
      // this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
         
            console.log(response.data);
            this.offerhistcount=response.data.totalcount;
            if(this.offerhistcount>5){
              this.ridehistshowbtn=false;
              

            }
          }
       
      })*/

  }
  booking_count(){
        var This = this;
       This.rootdb  = firebase.database().ref(); 
       This.rootdb.child('booking').on('value',function(snap){
           This.bookcount = snap.val(); //console.log(This.usercar);
              if(This.bookcount!=null && This.bookcount!='' ){
                   var new_array = new Array();
                  for(var i in This.bookcount){
                    if(This.bookcount[i].user_id==This.userid){
                    new_array.push(This.bookcount[i]);
                   }
                  }
                  This.bookcount = new_array;console.log(This.bookcount);
               if(this.bookcount>5){
              this.bookshowbtn=false;
            }
            }
          })
    	// this.myservice.show_loader();
     /*this.myservice.load_post(this.userid,"booking_ride_count_details").subscribe(response => {
	  	// this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
         
            console.log(response.data);
            this.bookcount=response.data.totalcount;
            console.log(this.bookcount);
            if(this.bookcount>5){
              this.bookshowbtn=false;

            }
          }
       
      })*/

  }
    booking_history_count(){
 /*     // this.myservice.show_loader();
     this.myservice.load_post(this.userid,"booking_history_count_details").subscribe(response => {
      // this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
         
            console.log(response.data);
            this.bookhistcount=response.data.totalcount;
            console.log(this.bookhistcount);
            if(this.bookhistcount>5){
              this.bookhistshowbtn=false;

            }
          }
       
      })*/

  }

  book_ride_details(){
/*    console.log(this.userid);
         var bookdata={'user_id':this.userid,
                     'start':''

      }
      //	this.myservice.show_loader();
      this.myservice.load_post(bookdata,"book_ride_details").subscribe(response => {
	  	//this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
         
            this.book_data=response.data;
            console.log(this.book_data);
                   this.book_data.forEach((element,j )=> {
                     this.get_source_book_split(element,j);

          this.book_data[j].source_head= this.source_sp[0];
          // this.book_data[j].source_head= 'kottayam';
           this.book_data[j].source_new= this.source_new;
            this.book_data[j].destination_head=  this.destination_head;
            this.book_data[j].destination_new=  this.destination_new;
            this.book_data[j].detour_time=this.formatted_time;
            this.book_data[j].reached_time=this.formatted_reached_time
            console.log( this.book_data);
             
           });
          }
       
      })*/
    }


    get_source_book_split(element,j){
           this.source_sp= element.departure_point.split(',');
           this.source_head=this.source_sp[0];         
           this.destination_sp= element.drop_point.split(', ',2);
           this.destination_head=this.destination_sp[0];
             let string_val1=element.destination;
             this.ride_time=element.detour_time.split(':');
             this.ampm='AM';
                  if ( this.ride_time[0] >= 12) {
                    this.ampm= 'PM';
                   }
                    if ( this.ride_time[0] > 12) {
                        this.ride_time[0] =  this.ride_time[0] - 12;
                    }
              this.formatted_time =  this.ride_time[0] + ':' +  this.ride_time[1] + ':' +  this.ride_time[2] + ' ' +this.ampm;
              this.reached_time = element.reached_time.split(':');
              console.log('reached',element.reached_time)
              this.ampm='AM';
                if (this.reached_time[0] >= 12) {
                  this.ampm= 'PM';
                 }
                if (this.reached_time[0] > 12) {
                  this.reached_time[0] =   this.reached_time[0] - 12;
              }
            this.formatted_reached_time =  this.reached_time[0] + ':' +  this.reached_time[1] + ':' +   this.reached_time[2] + ' ' +this.ampm;
            console.log(this.source_sp[0]);

    }
            // element.departure_point='kottayam,kerala,India';
           






  offer_ride_details(){
  /*    console.log(this.userid);
      var offerdata={'user_id':this.userid,
                     'start':''
                     

      }
      	//this.myservice.show_loader();
        this.myservice.load_post(offerdata,"list_offer_ride").subscribe(response => {
	  	//this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
           
          this.offerdata=response.data;
          this.offerdata.forEach((element,j )=> {
            
                  this.get_source_split(element,j);
              


            console.log(this.source_sp[0]);
            
           this.offerdata[j].source_head= this.source_head;
           this.offerdata[j].source_new= this.source_new;
           this.offerdata[j].destination_head=  this.destination_head;
           this.offerdata[j].destination_new=  this.destination_new;
           this.offerdata[j].detour_time= this.formatted_time;
           this.offerdata[j].reached_time= this.formatted_reached_time
            console.log(this.offerdata);
             
           });

               
            

          }
          // else {
          //   this.myservice.show_alert("Error!", response.message);
          // }
      }) 
     */
  }
  get_source_split(element,j){
    console.log(element,j);
    // alert('ys');

            // element.source='kottayam,kerala,India'
            this.source_sp= element.source.split(', ');
            this.source_head=this.source_sp[0];
            console.log(this.source_sp);           
              let string_val=element.source;
              let toArray = string_val.split(",");
              toArray.splice (0, 1);
              let str=toArray.toString();
              this.source_new=str;
              this.destination_sp= element.destination.split(', ',2);
              this.destination_head=this.destination_sp[0];
               let string_val1=element.destination;
                let toArray1 = string_val1.split(",");
                toArray1.splice (0, 1);
                let str1=toArray.toString();
             

             this.destination_new= str1;
             console.log('first_de',element.detour_time);
             console.log('first_re',element.reached_time);
            this.ride_time=element.detour_time.split(':');
            this.ampm='AM';
            if ( this.ride_time[0] >= 12) {
              this.ampm= 'PM';
          }
          if ( this.ride_time[0] > 12) {
            this.ride_time[0] =  this.ride_time[0] - 12;
        }
       this.formatted_time =  this.ride_time[0] + ':' +  this.ride_time[1] + ':' +  this.ride_time[2] + ' ' +this.ampm;
       this.reached_time=element.reached_time.split(':');
       console.log('reached',element.reached_time)
       this.ampm='AM';
       if (this.reached_time[0] >= 12) {
         this.ampm= 'PM';
     }
     if (this.reached_time[0] > 12) {
       this.reached_time[0] =   this.reached_time[0] - 12;
       console.log('reached1', this.reached_time[0])
   }
   this.formatted_reached_time =  this.reached_time[0] + ':' +  this.reached_time[1] + ':' +   this.reached_time[2] + ' ' +this.ampm;
   console.log('timestart',this.formatted_time);  
   console.log('timeend', this.formatted_reached_time);

            console.log(this.source_sp[0]);


  }
  ride_rate_user(){

  }

  ionViewDidLoad() {
    this.tab = "active";
  }

  presentPopover(myEvent,data,id,bookid,type) {
    console.log(myEvent);
    console.log(data);
   
let popover = this.popoverCtrl.create('Popovermore',{'data':data,'owner':id,'book_id':bookid,'type':type});
popover.present({
ev: myEvent,

});
popover.onDidDismiss(data => {
  
  this.navCtrl.setRoot(this.navCtrl.getActive().component);
  //this.nav.setRoot('Yourride');
  console.log(data);
 // this.item.rating=data.rate.rate;
//   this.viewCtrl.dismiss({'rate':data});
// //  this.navCtrl.push('Yourride');
// console.log(data);
});
}
  presentPopover1(myEvent,type,data) {
    console.log(myEvent);
    console.log(data);
    console.log(type);
   
let popover = this.popoverCtrl.create('Popovermore',{'type':type,'data':data});
popover.present({
ev: myEvent,

});
//popover.onDidDismiss(data => {
  
  //this.navCtrl.setRoot(this.navCtrl.getActive().component);
  //this.nav.setRoot('Yourride');
  console.log(data);
 // this.item.rating=data.rate.rate;
//   this.viewCtrl.dismiss({'rate':data});
// //  this.navCtrl.push('Yourride');
// console.log(data);
//});

}

showrides(item){
/*  this.offerdatalength=this.offerdata.length;
  console.log(this.offercount);
  console.log(this.offerdatalength);
  var length = this.offerdatalength+5
   if(length>= this.offercount){
              this.rideshowbtn =true;

            }
      var offerdata={'user_id':this.userid,
                     'start':this.offerdata.length
                     

      }
      //this.myservice.show_loader();
        this.myservice.load_post(offerdata,"list_offer_ride").subscribe(response => {
	  	//this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
           response.data.forEach((element,j)=> {
            this.get_source_split(element,j);

          element.source_head= this.source_head;
          element.source_new= this.source_new;
          element.destination_head=  this.destination_head;
          element.destination_new=  this.destination_new;
          element.detour_time= this.formatted_time;
          element.reached_time= this.formatted_reached_time

             this.offerdata.push(element);


              
           });
            // this.offerdata.push(response.data);
            console.log(this.offerdata);
           
            

          }
          // else {
          //   this.myservice.show_alert("Error!", response.message);
          // }
      })*/ 

}
showrides_history(){
/*    this.offerdatalength=this.offerhistory.length;
  console.log(this.offerhistcount);
   console.log(this.offerdatalength);
   if(this.offerdatalength+5 >= this.offerhistcount){
     // this.myservice.show_alert("Warning!", "No More Data To Show");
              this.ridehistshowbtn =true;

            }
      var offerdata={'user_id':this.userid,
                     'start':this.offerhistory.length
                     

      }
      this.myservice.show_loader();
        this.myservice.load_post(offerdata,"offer_history_ride").subscribe(response => {
      this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
           response.data.forEach((element,j )=> {
              this.get_source_split(element,j);

          element.source_head= this.source_head;
         element.source_new= this.source_new;
           element.destination_head=  this.destination_head;
          element.destination_new=  this.destination_new;
          element.detour_time= this.formatted_time;
          element.reached_time= this.formatted_reached_time
              this.offerhistory.push(element);
           });
            // this.offerdata.push(response.data);
            console.log(this.offerhistory);
           
            

          }
          // else {
          //   this.myservice.show_alert("Error!", response.message);
          // }
      }) */

}
  showbooking(){
/*      this.bookdatalength=this.book_data.length;
  console.log(this.bookdatalength);
   console.log(this.bookcount);
   if(this.bookdatalength +5 >= this.bookcount){

              this.bookshowbtn =true;

            }
        var bookdata={'user_id':this.userid,
                     'start':this.book_data.length
                     

      }
      this.myservice.show_loader();
        this.myservice.load_post(bookdata,"book_ride_details").subscribe(response => {
	  	this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
           response.data.forEach((element,j) => {
              this.get_source_book_split(element,j);

          element.source_head= this.source_head;
          element.source_new= this.source_new;
          element.destination_head=  this.destination_head;
          element.destination_new=  this.destination_new;
          element.detour_time= this.formatted_time;
          element.reached_time= this.formatted_reached_time
              this.book_data.push(element);
           });
            // this.offerdata.push(response.data);
           
            console.log(this.book_data);
           
            

          }
          // else {
          //   this.myservice.show_alert("Error!", response.message);
          // }
      }) */

}
showbooking_history(){

  /* this.bookdatalength= this.book_history.length;
  console.log(this.bookdatalength);
   console.log(this.bookcount);
  //   alert(this.bookdatalength);
  // alert(this.bookhistcount);
   if(this.bookdatalength+5>= this.bookhistcount){

              this.bookhistshowbtn =true;

            }
        var bookdata={'user_id':this.userid,
                     'start': this.book_history.length
                     

      }
      this.myservice.show_loader();
        this.myservice.load_post(bookdata,"booking_ride_history").subscribe(response => {
      this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
           response.data.forEach((element,j) => {
              this.get_source_book_split(element,j);

          element.source_head= this.source_head;
         element.source_new= this.source_new;
           element.destination_head=  this.destination_head;
          element.destination_new=  this.destination_new;
          element.detour_time= this.formatted_time;
          element.reached_time= this.formatted_reached_time
               this.book_history.push(element);
           });
            // this.offerdata.push(response.data);
           
            console.log( this.book_history);
           
            

          }
          // else {
          //   this.myservice.show_alert("Error!", response.message);
          // }
      }) */

}
  offerride() {
  this.navCtrl.push('Offerride');
}

findride() {
this.navCtrl.push('Findride');
}

profile() {
this.navCtrl.push('Profile');
}
startride(item){
  console.log(item);
  this.navCtrl.push('StartdrivePage',{'data':item});
}

tab_swap(type) {
    this.tab = type;
   }

   message() {
   
   this.navCtrl.push('Message',{'data':this.book_data});
  }
 booked_status(item){
  console.log(item);
  this.navCtrl.push('UsertrackPage',{'data':item});

 }

}
