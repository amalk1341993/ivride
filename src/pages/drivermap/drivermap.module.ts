import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Drivermap } from './drivermap';

@NgModule({
  declarations: [
    Drivermap,
  ],
  imports: [
    IonicPageModule.forChild(Drivermap),
  ],
  exports: [
    Drivermap
  ]
})
export class DrivermapModule {}
