import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Drop } from './drop';

@NgModule({
  declarations: [
    Drop,
  ],
  imports: [
    IonicPageModule.forChild(Drop),
  ],
  exports: [
    Drop
  ]
})
export class DropModule {}
