import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,NavParams,ToastController} from 'ionic-angular';
import { FirebaseService } from '../../providers/firebaseservice';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html'
})
export class Message {
tab:any;
data:any;
msg_item:any;
userid:any;
msgdisplay:any;
msgdetails:any;
userdata:any;
display:any=[];
chatuserprofile:any;
 pushdata:any;
newpushdata:any;
msglength:any;
pushdata_entry:any;
newpushdata_entry:any;
chat_user_id:any;
rootdb:any;
uid:any;
currentUser: any;
booking:any;
notifications:any;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController,private myservice: Myservice,public navParams: NavParams,public firebaseService:FirebaseService,private toastCtrl: ToastController) {
    this.newpushdata_entry = navParams.get("data");
    this.pushdata_entry= navParams.get("push");

    
    
    console.log('push',this.pushdata_entry)
    if(this.pushdata_entry){
      this.tab = "inactive";
    }else{
      this.tab = "active";
    }
    console.log('msgdata',this.newpushdata);

         var This = this;
        this.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
          console.log(users);
          This.uid = users.uid;
          This.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.currentUser = snap.val();
           console.log();
                   setTimeout(() =>{
                     This.userdata=This.currentUser;
                  This.userid =users.uid;
                  console.log(This.userid);
                    This. unread_msg();
                    This.notification_load();
                     This.loadmsg()
                   },1000)
            
          })
        }
 })

   var users = this.firebaseService.getCurrentUser();     

   this.rootdb.child('notify_data').child(users.uid).on('value',function(snap){
     This.notifications = This.myservice.obj_array(snap.val());
   })

  }

   ionViewDidLoad() {
  
   
  }

  tab_swap(type) {
      this.tab = type;
     }



     remove_not(id){
       var users = this.firebaseService.getCurrentUser(); 
       this.rootdb.child('notify_data').child(users.uid).child(id).remove();
       let toast = this.toastCtrl.create({
              message: "Removed Successfully",
              duration: 2000,
              position: 'bottom'
            });
            toast.present();

     }



     dismiss() {
      this.navCtrl.push('Home');
    }

    profile() {
    this.navCtrl.push('Profile');
  }
  approve(bookdata){
     this.navCtrl.push('Manage',{'data':bookdata});

  }
  notification_load(){

var This=this;
var users = this.firebaseService.getCurrentUser();
//this.uid = users.uid;
        This.rootdb.child('booking').on('value',function(snap){
           This.booking = snap.val(); //console.log(This.usercar);
              if(This.booking!=null && This.booking!='' ){
                   var new_array = new Array();
                  for(var i in This.booking){
                    if(This.booking[i].user_id==users.uid){
                    new_array.push(This.booking[i]);
                   }
                  }
                  This.newpushdata = new_array;
            }
          })


    /*this.myservice.load_post(this.userid ,"get_push_notification_details").subscribe(response => {
      //if(response.status == "success") {
    
        this.newpushdata = response;
        console.log(this.newpushdata);

     // }



    })*/
  }
   unread_msg(){
     console.log(this.userid );
      var updateurll = "msg_unread/" + this.userid  + "/" ;
           var This=this;
           This.rootdb  = firebase.database().ref();
            This.rootdb.child(updateurll).on('value',function(snap){
            This.msg_item = snap.val();
            console.log(This.msg_item);



        /*this.firebaseService.checkItem(updateurll).subscribe((item) => {
            console.log('item',item)
              console.log('item',item.$value)
               console.log('item',item.count)
                console.log('item',item.key)
               this.msg_item=item.count;
               console.log(this.msg_item);*/
              })
   }
   loadmsg(){
     var This = this;
     var m = 0;
     var url = "recentmsg/" + this.userid + "/";
            var query = {
              // Child:'msg',
               orderByChild: 'timestamp'


            };
           
            // var This = this;
            this.msgdisplay = new Array();
             var This=this;
              This.rootdb  = firebase.database().ref();
            This.rootdb.child(url).limitToLast(10).on('value',function(snap){
            var arrlen = snap.val().length;
            var data = This.myservice.obj_array(snap.val());
            console.log(data);
            console.log(data.length);



            This.msglength = data.length;
             ///this.firebaseService.listrecentItem(url).subscribe((item) => {
                //var arrlen = item.length;
                 
                 //console.log(item,arrlen);
                 //this.msglength = data.length;
                 console.log(This.msglength);

                 data.forEach((element,j)  => {
                  if(element.To_id==This.userid){
                    var updateurlcheck = "msg_unread/" + element.From_id  + "/" + element.To_id;
                    console.log(updateurlcheck);
                    This.rootdb.child(updateurlcheck).on('value',function(item){
                      console.log('item',item.val())
                         var data_item = item.val();
                         console.log('item',data_item.count)
                        
                         This.msg_item = data_item.count;
                         element.count=This.msg_item;
                        })

                  }else if(element.To_id!=this.userid){
                    var updateurlcheck = "msg_unread/" + element.To_id  + "/" + element.From_id;
                     console.log(updateurlcheck);
                     This.rootdb.child(updateurlcheck).on('value',function(items){
                       var item = items.val();
                      console.log('item',item)
                     
                         console.log('item',item.count)
                        
                         This.msg_item=item.count;
                         element.count=This.msg_item;
                         console.log(This.msg_item);
                        })

                  }
                  console.log(This.msg_item);
             
                 
                    if(element.To_id==This.userid){
                      This.chat_user_id=element.From_id
                       console.log(This.chat_user_id);
                    } if(element.To_id!=This.userid){
                      This.chat_user_id=element.To_id;
                      
                       console.log(This.chat_user_id);
                    }
                    console.log(This.chat_user_id);


            This.rootdb.child('users').child(This.chat_user_id).once('value',function(snap){


                      element.user = snap.val();
                      console.log(element);
                       //This.msgdetails = element;
                  

          });

                  
 
This.msgdisplay.push(element);

This.msgdetails = This.msgdisplay;

console.log(This.msgdisplay);


                /*m = m+1;
                if(arrlen == m) {
                  This.msgdetails = snap;
                }*/

                })
                
                
             
            });
          

   }
 
   checkclick(item){
     console.log(item);
     if(this.userid==item.From_id){
      var update_url = "msg_unread/" + item.To_id + "/"+ item.From_id ;
      var count_msg={"count":0}
      var chatdata={'From_id':item.From_id,
      'To_id':item.To_id}
     }else{
      var update_url = "msg_unread/" + item.From_id + "/"+ item.To_id ;
      var count_msg={"count":0}
      var chatdata={'From_id':item.To_id,
      'To_id':item.From_id}
     }
    
     this.firebaseService.createItem(update_url,count_msg);

    
this.navCtrl.push('Chat',{'data':chatdata,'userdata':this.userdata});

   }



               

}
