import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Dropex } from './dropex';

@NgModule({
  declarations: [
    Dropex,
  ],
  imports: [
    IonicPageModule.forChild(Dropex),
  ],
  exports: [
    Dropex
  ]
})
export class DropexModule {}
