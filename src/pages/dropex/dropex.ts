import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-dropex',
  templateUrl: 'dropex.html'
})
export class Dropex {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {}

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss();
   }

}
