import { Component } from '@angular/core';
import {IonicPage,NavController,ViewController,ToastController } from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {
private forgot : FormGroup;
auth:any;
email:any;
rootdb:any;
users:any;
  constructor(	public navCtrl: NavController,
  				public viewCtrl: ViewController,
  				private myservice: Myservice, 
  				private formBuilder: FormBuilder,
  				private toastCtrl: ToastController,
  				public afAuth: AngularFireAuth, 
  			) 
			 {
				this.forgot = this.formBuilder.group({ 
					email: ['', Validators.required]
				});
			 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }
    forgot_password() {
       var email = this.forgot.value.email;
       var This=this;
       This.rootdb  = firebase.database().ref();
       This.rootdb.child('users').orderByChild('email').equalTo(email).once('value',function(snap){
       //This.users = snap.val();
       if(snap.exists()){
         var auth = firebase.auth();
	       var res =  auth.sendPasswordResetEmail(email)
	      .then(() => console.log("email sent"))
	      .catch((error) => console.log(error))
       if(res){
       	 This.viewCtrl.dismiss();
       }
    }
    else{
      let toast = This.toastCtrl.create({
        message: 'Email not exist',
        duration: 2000,
        position: 'bottom'
      });
        toast.present();
    }
       })
  }
    dismiss() {
	this.viewCtrl.dismiss(); 
  }

}
