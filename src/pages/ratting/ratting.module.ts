import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ratting } from './ratting';

@NgModule({
  declarations: [
    Ratting,
  ],
  imports: [
    IonicPageModule.forChild(Ratting),
  ],
  exports: [
    Ratting
  ]
})
export class RattingModule {}
