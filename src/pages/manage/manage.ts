import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-manage',
  templateUrl: 'manage.html'
})
export class Manage {
  seat_num:any;
  data:any;
  date:any;
  year:any;
  date_of_birth:any;
  age:any;
  leftseats:any;
  userdata:any;
  rootdb:any;
dob:any;
car:any;
ride:any;
  constructor(   public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
              ) {
					 
					 
		 var This = this;
        this.rootdb  = firebase.database().ref();
          var users = this.firebaseService.getCurrentUser();
           this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.userdata = snap.val();
        
          })	  
     this.data = navParams.get("data");
     if(this.data.dob){
          this.date=new Date();
              this.year= this.date.getFullYear();

                this.dob= this.data.dob;
                 console.log(this.dob);
                var values = this.dob.split("-");
			        	console.log(values);
                this.age =this.year-values [0];
                console.log(this.age);
                console.log(this.dob);
     }
     this.leftseats=this.data.rideseat-this.data.no_of_seats;
     console.log(this.data);
  }

  ionViewDidLoad() {
  }

  goBack(){
     this.navCtrl.pop();
   }
   contact(){
     this.navCtrl.push('Chat',{'data':{'From_id':this.userdata.id,'To_id':this.data.user_id},'userdata':this.userdata});
     
   }
   approve(){
     var users = this.firebaseService.getCurrentUser();
     //var approvedata={'booking_id':this.data.booking_id,'no_of_seats':this.leftseats,'ride_id':this.data.ride_id,'user_id':this.data.user_id,'driver_id':this.data.id}
       //this.myservice.show_loader();
       var approvedata={'status':1};
       this.rootdb.child('booking').child(this.data.booking_id).update(approvedata);
       var approveride={'no_of_seats':this.leftseats};
       this.rootdb.child('ride').child(this.data.ride_id).update(approveride);
	  	//this.myservice.hide_loader();
      this.navCtrl.push('Yourride');
   }
   decline(){
      ////his.myservice.show_loader();
       var approvedata={'status':2};
       this.rootdb.child('booking').child(this.data.booking_id).update(approvedata);
       this.navCtrl.push('Yourride'); 
   }
   delete(){
      //this.myservice.show_loader();
      var url="ride/"+this.data.ride_id;
      this.firebaseService.removeItem(url);
      //this.rootdb.child('ride').child(this.data.ride_id).removeValue();
	  	//this.myservice.hide_loader();
     this.navCtrl.push('Yourride');
   }
   edit(id){

       var This = this;
       This.rootdb  = firebase.database().ref(); 
       This.rootdb.child('ride').child(id).on('value',function(snap){
           This.ride = snap.val(); //console.log(This.usercar);
             This.rootdb.child('car_details').orderByChild('car_id').equalTo(This.ride.car_id).once('value',function(snap){
              This.car = snap.val();
          })
        })



     	//this.myservice.hide_loader();
            this.navCtrl.push('OffereditPage',{'data': this.car});
         
            
           }     
    


}
