import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ModalController,NavParams,Platform  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {FirebaseService} from '../../providers/firebaseservice';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { Geolocation } from '@ionic-native/geolocation';
import * as firebase from 'firebase';
@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class Contact {
  data:any;
  seat_nw:any;
  userdata:any;
  msg:any;
  id1:any;
  id2:any;
  unread:any;
  location:any;
  locationmsg:any;
  data_location:any;
  rate:any;
  msg_item:any;
rootdb:any;
date:any;
year:any;
userid:any;
source:any;
destination:any;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController,
              public modalCtrl: ModalController,public navParams: NavParams,public firebaseService:FirebaseService,
              public storage:Storage,private platform: Platform, private geolocation: Geolocation) {
      this.data= navParams.get("data");console.log(this.data);
       this.rate= navParams.get("rate");
      /* var source1 =  this.data.source;
          var x = source1.split(",");
          this.source = x[0];
          var destination1 = this.data.destination;
          var y= destination1.split(",");
          this.destination = y[0];*/



        var This = this;
        This.rootdb  = firebase.database().ref();
        var users = This.firebaseService.getCurrentUser();//console.log(users.uid);
        this.userid = users.uid;
        this.userdata=users;
        this.unread_msg();

      This.rootdb.child('users').child(this.userid).once('value',function(snap){
         //This.find_data.users = snap.val();
         var users = snap.val();
          /*This.date=new Date();
          This.year= This.date.getFullYear();
          if(users.dob){
            var dob = users.dob.split("-");
            users.age = This.year-dob[2]+" Y/O";
          } else {
            users.age = '';
          }*/
          This.userdata = users;console.log(This.userdata);

           
        
       })
     
    
     console.log(this.data);
         platform.ready().then(() => {
             var This=this;
      geolocation.getCurrentPosition().then((location) => {
      
        console.log(location);
      
        This.location=location;
         console.log(this.location.coords.latitude);

        
      }).catch((error) => {
      
        console.log('Error getting location', error);
        
      });
       this.location=This.location;
      
    } );
   
    //  console.log(this.location);
  
  }
  

  ionViewDidLoad() {
  }
    unread_msg(){
      var This = this;
        This.rootdb  = firebase.database().ref();
        var users = This.firebaseService.getCurrentUser();//console.log(users.uid);
        This.userid = users.uid;

      this.id1 = This.userid ;
      this.id2 = this.data.users_id;
      console.log( this.id1);
      console.log(  this.id2 );

      var updateurll = "msg_unread/" + this.id2 + "/" + this.id1 + "/";
        /*this.firebaseService.checkItem(updateurll).subscribe((item) => {
            console.log('item',item)
              console.log('item',item.$value)
               console.log('item',item.count)
               this.msg_item=item.count;
        })*/
	}

    bookpopup() {
   let modal = this.modalCtrl.create('Bookpopup',{'data':this.data,'rate': this.rate});
   modal.present();
   modal.onDidDismiss(newdata => {
         console.log(newdata);
           this.navCtrl.push('Secureseat',newdata);
          })
   }

   chat() {
      var This = this;
        This.rootdb  = firebase.database().ref();
     var users = This.firebaseService.getCurrentUser();
     this.id2 = users.uid;
     this.id1 = this.data.users_id;
   
      var msg = {
        "From_id": this.id2,
        "To_id": this.id1,
        "message": this.msg,
        "timestamp": firebase.database.ServerValue.TIMESTAMP,
        "type":'text'
       
      }
       
    
        console.log(msg);

        if (this.id1 < this.id2) {
          console.log(this.id1);
          console.log(this.id2);

          var url = "chats/" + this.id1 +'-'+ this.id2 + "/";

          this.firebaseService.pushItem(url, msg);
        
           var recenturl="recentmsg/"+ this.id1+'/' +this.id1 +'-'+ this.id2 + "/";
            this.firebaseService.createItem(recenturl, msg);
            var recenturl="recentmsg/"+ this.id2+'/' +this.id1 +'-'+ this.id2 + "/";
            this.firebaseService.createItem(recenturl, msg);
              this.msg=''

      
    } else {
      var url = "chats/" + this.id2 +'-'+ this.id1 + "/";
      
      this.firebaseService.pushItem(url, msg);
       var recenturl="recentmsg/"+ this.id1+'/' +this.id2 +'-'+ this.id1 + "/";
        this.firebaseService.createItem(recenturl, msg);
       var recenturl="recentmsg/"+ this.id2+'/' +this.id2 +'-'+ this.id1 + "/";
        this.firebaseService.createItem(recenturl, msg);
         this.msg=''
      }
      console.log(this.msg_item);
        if (this.msg_item == undefined) {
          
    //  alert('ys0')
    console.log(this.id1);
    console.log(this.id2);
        var updateurll = "msg_unread/" + this.id2 + "/" + this.id1 + "/";
                   var count_msg={"count":1}
            

            this.firebaseService.createItem(updateurll,count_msg);

        } else {
          var updateurll = "msg_unread/" + this.id2 + "/" + this.id1 + "/";

              var count1_msg={"count":this.msg_item + 1}



            this.firebaseService.createItem(updateurll,count1_msg);
        }

     
  let modal = this.modalCtrl.create('Chat',{'data':msg,'userdata':this.userdata});
  modal.present();
}


  dismiss() {
   this.viewCtrl.dismiss();
  }
 locationselect(){
  this.id1 =this.data.user_id;
  this.id2 =this.userdata.id;
   
   this.locationmsg={'lattitude':this.location.coords.latitude,'longitude':this.location.coords.longitude}
   var msg= {"From_id": this.id2,
        "To_id": this.id1,
        "message": this.locationmsg,
        "timestamp": firebase.database.ServerValue.TIMESTAMP,
        "type":'location'}
        console.log(msg);
         if (this.id1 < this.id2) {

          var url = "chats/" + this.id1 +'-'+ this.id2 + "/";
          this.firebaseService.pushItem(url, msg);
        
           var recenturl="recentmsg/"+ this.id1+'/' +this.id1 +'-'+ this.id2 + "/";
            this.firebaseService.createItem(recenturl, msg);
            var recenturl="recentmsg/"+ this.id2+'/' +this.id1 +'-'+ this.id2 + "/";
            this.firebaseService.createItem(recenturl, msg);
              this.msg=''
    } else {
      var url = "chats/" + this.id2 +'-'+ this.id1 + "/";
      
      this.firebaseService.pushItem(url, msg);
       var recenturl="recentmsg/"+ this.id1+'/' +this.id2 +'-'+ this.id1 + "/";
        this.firebaseService.createItem(recenturl, msg);
       var recenturl="recentmsg/"+ this.id2+'/' +this.id2 +'-'+ this.id1 + "/";
        this.firebaseService.createItem(recenturl, msg);
         this.msg=''
      }
       if (this.msg_item == undefined) {
        //  alert('ys0')
        var updateurll = "msg_unread/" + this.id2 + "/" + this.id1 + "/";
                   var count_msg={"count":1}
            

            this.firebaseService.createItem(updateurll,count_msg);

        } else {
          var updateurll = "msg_unread/" + this.id2 + "/" + this.id1 + "/";
         
              var count1_msg={"count":this.msg_item + 1}



            this.firebaseService.createItem(updateurll,count1_msg);
        }

       let modal = this.modalCtrl.create('Chat',{'data':msg,'userdata':this.userdata});
  modal.present();
 }
 get_loc(loc){
    var y = loc.split(",");
    return y[0];
  }
  get_left_loc(loc){
    let toArray = loc.split(",");
    toArray.splice (0, 2);
    return toArray.toString();
  }

}
