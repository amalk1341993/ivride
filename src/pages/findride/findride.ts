import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams } from 'ionic-angular';
declare var google;
declare var require: any;
@IonicPage()
@Component({
  selector: 'page-findride',
  templateUrl: 'findride.html'
})
export class Findride {
  findride:FormGroup;
  place_from:any;
  lat_from:any;
  long_from:any;
  place_to:any;
  lat_to:any;
  long_to:any;
  seats:any;
  today:any;
  date:any;
  max_date:any;
  tom:any;
  month1:any;
  month2:any;
  userdata:any;
  user_id:any;
rootdb:any;
ride:any;
charge:any;

  constructor(
              public popoverCtrl: PopoverController,
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              public storage: Storage, 
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public navParams: NavParams
    ) {
    firebase.database().ref('ibkar').push();
    this.findride = formBuilder.group({
      'ride_from' : ['', Validators.required],
      'ride_to':['', Validators.required],
      'ride_date':['', Validators.required],
       'rtrnride_date':['', Validators.required],
      
    });
     var This = this;
        This.rootdb  = firebase.database().ref();
          var userss = This.firebaseService.getCurrentUser();
           /*This.rootdb.child('users').child(userss.uid).on('value',function(snap){
            This.userdata = snap.val();
        
          })*/
    var myDate = new Date();
    var nextDay = new Date(myDate);
    nextDay.setDate(myDate.getDate()+1);
    this.tom = nextDay.toISOString();
    this.month1=this.tom ;

       
     var profits=2489.8237
profits.toFixed(3) //returns 2489.824 (round up)
profits.toFixed(2)
//console.log(profits.toFixed(3));
this.today=new Date();
//console.log(this.today);
this.date = this.today.getFullYear()+'-'+(this.today.getMonth()+1)+'-'+this.today.getDate();
//console.log(this.date)
this.max_date = new Date(); 
  }

  ionViewDidLoad() {
    var lat1 = 10.0237;
    var lon1 = 76.3116;

    var lat2 = 10.0152;
    var lon2 = 76.3294;

    var distances = Math.round(this.getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) * 100) / 100;

    console.log(distances);

    var distance = require('google-distance-matrix');

    var origins = ['San Francisco CA', '40.7421,-73.9914'];
    var destinations = ['New York NY', 'Montreal', '41.8337329,-87.7321554', 'Honolulu'];
     //AIzaSyAkBTIyrGkQpBPRKhhuN72bzq7MJFdKlVk
    distance.key('AIzaSyBniwyiEPSjS9IUvbhBmS0ampJkzE2cSS0');
    distance.units('imperial');
     
    distance.matrix(origins, destinations, function (err, distances) {
        if (err) {
            return console.log(err);
        }
        if(!distances) {
            return console.log('no distances');
        }
        if (distances.status == 'OK') {
            for (var i=0; i < origins.length; i++) {
                for (var j = 0; j < destinations.length; j++) {
                    var origin = distances.origin_addresses[i];
                    var destination = distances.destination_addresses[j];
                    if (distances.rows[0].elements[j].status == 'OK') {
                        var distance = distances.rows[i].elements[j].distance.text;
                        console.log('Distance from ' + origin + ' to ' + destination + ' is ' + distance);
                    } else {
                        console.log(destination + ' is not reachable by land from ' + origin);
                    }
                }
            }
        }
    });

  }


  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in kilometers
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in KM
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }


  seatFn(event){
      var target = event.target 
      // event.srcElement;
      this.seats = event.target.innerHTML;
      //console.log(this.seats);
  }
  autocompleteFocusfrom(){
    var input = document.getElementById('ridefrom');
    var options = {
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
    var This=this;
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      var place = autocomplete.getPlace();
      This.place_from = place.formatted_address;  
      This.lat_from = place.geometry.location.lat();console.log(This.lat_from);
      This.long_from = place.geometry.location.lng();console.log(This.long_from);
    });
    this.place_from = This.place_from;
    this.lat_from= This.lat_from;
    //console.log(this.lat_from);
    this.long_from = This.long_from;
   }

  autocompleteFocusto(){
    var input = document.getElementById('rideto');
    var options = {
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    var This=this;
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      var place = autocomplete.getPlace();
      This.place_to= place.formatted_address;
      This.lat_to = place.geometry.location.lat();
      This.long_to = place.geometry.location.lng();
    });
    this.place_to= This.place_to;
    this.lat_to= This.lat_to;
   // console.log(this.lat_to);
    this.long_to = This.long_to;
    }

  searchresult() {
  this.navCtrl.push('Searchresult');
}
findrideForm(event,post){


//console.log(post);
 // console.log(this.seats)
  /*if(this.userdata){
     var users = this.firebaseService.getCurrentUser();
    this.user_id=users.uid;

  }else{
     this.user_id='undefined';
  }*/
  if(this.seats==undefined){    
      this.myservice.show_alert("Required", 'please fill the seats ');
  }else{

      var dep_date = post.ride_date.split("T");
      post.ride_date = dep_date[0];


          var source = this.place_from;
          var x = source.split(",");
         // console.log(x[0]);
          var destination = this.place_to;
          var y= destination.split(",");
         // console.log(y[0]);
     
     var data={
               'destination':this.place_to,
               'source':this.place_from,  
               'departure_date':post.ride_date,
               'rtrn_time':post.rtrnride_date,
               'seats':this.seats
     }

     this.navCtrl.push('Searchresult',{'data': data});




/*



     //console.log(data);
  var date = post.ride_date;
  var returntime=post.rtrnride_date;
   var This = this;
          
           //This.rootdb.child('ride').orderByChild('source').equalTo(x[0]).orderByChild('destination').equalTo(y[0]).orderByChild('departure_date').equalTo(date).orderByChild('rtrn_time').equalTo(returntime).once('value',function(snap){
              This.rootdb.child('ride').orderByChild('source').equalTo(source).once('value',function(snap){
              This.ride = snap.val(); console.log(This.ride);
           /*   if(This.ride!=null && This.ride!='' ){
                   var new_array = new Array();
                  for(var i in This.ride){
                      if(This.ride[i].no_of_seats>0){
                        new_array.push(This.ride[i]);
                   }
                  }
                  This.ride = new_array;
                  console.log(This.ride);
            }*/

/*
            This.navCtrl.push('Searchresult',{
               'firstPassed': This.ride,
                'secondPassed': data
                //'thirdpassed':response.time,
               
         
    });
          })


/*var origin1 = new google.maps.LatLng( This.lat_from,  This.lat_to);
var origin2 = source;
var destinationA = destination;
var destinationB = new google.maps.LatLng(This.lat_to, This.long_to);

var service = new google.maps.DistanceMatrixService();
service.getDistanceMatrix(
  {
    origins: [origin1, origin2],
    destinations: [destinationA, destinationB],
    travelMode: 'DRIVING',
    avoidHighways: Boolean,
    avoidTolls: Boolean,
  });*/







  





            



           //this.myservice.show_loader();
  
    /*  this.myservice.load_post(data, "find_ride").subscribe(response => {
      //this.myservice.hide_loader();

     console.log(response);
      console.log(response.time);
          if(response.status == "success") {
            console.log(response.data);
            console.log(response.check);
            let toast = this.toastCtrl.create({
              message: response.message,
              duration: 2000,
              position: 'bottom'
            });
              toast.present();
            this.navCtrl.push('Searchresult',{
               'firstPassed': response.data,
                'secondPassed': data,
                'thirdpassed':response.time,
                 //'fourthpassed':response.charge
         
    });
          }else if(response.status == "failed"){
             this.navCtrl.push('Searchresult',{
            
                'secondPassed': data,
    });
          }
         })*/



          }
  
}
  goBack(){
     this.navCtrl.pop();
   }
}
