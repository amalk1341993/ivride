import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Findride } from './findride';

@NgModule({
  declarations: [
    Findride,
  ],
  imports: [
    IonicPageModule.forChild(Findride),
  ],
  exports: [
    Findride
  ]
})
export class FindrideModule {}
