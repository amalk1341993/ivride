import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Pickex } from './pickex';

@NgModule({
  declarations: [
    Pickex,
  ],
  imports: [
    IonicPageModule.forChild(Pickex),
  ],
  exports: [
    Pickex
  ]
})
export class PickexModule {}
