import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pickex',
  templateUrl: 'pickex.html'
})
export class Pickex {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {}

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss();
   }

}
