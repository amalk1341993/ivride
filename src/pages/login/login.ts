import { Component,ViewChild } from '@angular/core';
import { IonicPage,Nav,NavController,ModalController,ViewController,Events,NavParams,ToastController,Platform } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { OneSignal } from '@ionic-native/onesignal';
import { Geolocation } from '@ionic-native/geolocation';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
	 @ViewChild(Nav) nav: Nav;
	private login : FormGroup;
	email:any;
	password:any;
    player_id:any;
    rootdb:any;
    location:any;
    longitude:any;
    lattitude:any;
	 constructor(public navCtrl: NavController,
	 			 public modalCtrl: ModalController,
	 			 private toastCtrl: ToastController,
	 			 private myservice: Myservice,
	 			 public storage: Storage,
	 			 private formBuilder: FormBuilder,
	 			 public firebaseService:FirebaseService,
	 			 public viewCtrl: ViewController,
	 			 public events: Events,
	 			 public navParams: NavParams,
	 			 private afAuth: AngularFireAuth,
	 			 private geolocation: Geolocation,
	 			 public oneSignal: OneSignal
		
	 			 ) {
				 	this.login = this.formBuilder.group({ 
						email: ['', Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])],
			      		password: ['', Validators.required]
					});

					 this.oneSignal.getIds().then(ids => { 
					    var This=this;
					    console.log(ids['userId']);
              console.log("hi i'm here");
					    This.player_id=ids['userId'];
					    console.log(This.player_id);
					    this.player_id=  This.player_id;
					    console.log(this.player_id);
					 });


	 			}

  ionViewDidLoad() {
  }
   home(){
   	var This=this;
   	//This.myservice.hide_loader();
   	this.email= this.login.value.email; console.log(this.email);
   	this.password= this.login.value.password;
		firebase.auth().signInWithEmailAndPassword(this.email,this.password).then(result => {
            console.log(result);
             var current_user = this.afAuth.auth.currentUser;console.log(current_user);
                var userObj ={  
	                player_id:this.player_id
               };
               this.rootdb  = firebase.database().ref();
              var res = this.rootdb.child('users').child(current_user.uid).update(userObj);
              
            //this.myservice.show_loader();
            this.navCtrl.push('Home'); 
        }).catch(function(error) {
		   console.log(error.code);
		   console.log(error.message);
		   let toast = This.toastCtrl.create({
				message: 'Invalid Credential',
				duration: 2000,
				position: 'bottom'
			});
		    toast.present();
			This.login.reset();
			
		});
	}
   




goBack(){
  const index = this.navCtrl.getActive().index;
  console.log(index);
  if(index==0){
      this.navCtrl.push('Home');
  } else {
    this.navCtrl.pop();
  }
 }

 forgotpassword() {
 	let modal = this.modalCtrl.create('ForgotpasswordPage');
 	modal.present();
 }
}
