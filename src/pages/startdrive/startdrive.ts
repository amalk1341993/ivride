import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-startdrive',
  templateUrl: 'startdrive.html',
})
export class StartdrivePage {

  data:any;
  source_sp:any;
  source_head:any;
  source_new:any;
  destination_sp:any;
  destination_head:any;
  destination_new:any;
  cardetails:any;
  cotravellers_list:any;
  userdata:any;
  num:any;
 rootdb:any;
 find_data:any;
 date:any;
 year:any;
 count:any;
  constructor( public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
                 ,private callNumber: CallNumber) {
     this.data=navParams.get("data");
     console.log(this.data);
     
      var This = this;
        This.rootdb  = firebase.database().ref();
        var users = This.firebaseService.getCurrentUser();//console.log(users.uid);
        this.userdata = users.uid;



     if(this.data){
        this.source_sp= this.data.source.split(',');
            this.source_head=this.source_sp[0];
            console.log(this.source_sp);           
              let string_val=this.data.source;
              let toArray = string_val.split(",");
              toArray.splice (0, 1);
              let str=toArray.toString();
              this.source_new=str;
              console.log(this.source_new);
            this.destination_sp= this.data.destination.split(',',2);
            this.destination_head=this.destination_sp[0];
             let string_val1=this.data.destination;
              let toArray1 = string_val1.split(",");
              toArray1.splice (0, 1);
              let str1=toArray.toString();
             

             this.destination_new= str1;
             console.log(this.destination_new);

             this.rootdb.child('car_details').child(this.data.car_id).once("value",function(snap){
               This.cardetails = snap.val();
               console.log(This.cardetails);
             })

     }
    //this.cardetails_ride();
    this.cotravellers();
  }
  cardetails_ride(){
    var data={'ride_id':this.data.id,
               'user_id':this.userdata,
               'driver_id':this.data.users_id,
               'status':1
              }
   this.rootdb.child('ride_status').child(this.userdata).update(data);


     /*this.myservice.load_post(data,"car_details_start").subscribe(response => {
       this.cardetails=response.data;
       console.log(this.cardetails);

     })*/

  }
  cotravellers(){
    var This = this;
    This.cotravellers_list = new Array();
     This.rootdb  = firebase.database().ref();
    This.rootdb.child('booking').orderByChild('ride_id').equalTo(this.data.id).once('value',function(snap){
         var co_trav = This.myservice.obj_array(snap.val());
         console.log(co_trav);
         //This.cotravellers_list
         

         co_trav.forEach((item,index) => {

            This.rootdb.child('users').child(item.user_id).once('value',function(data){
              This.cotravellers_list.push(data.val());
            })
          })


         //var cotravellers_list = This.myservice.obj_array(snap.val());
         //This.result = This.myservice.obj_array(preference);
          //This.count = rides.length;
       })



     /*this.myservice.load_post(this.data.id,"cotravellers_list").subscribe(response => {
       this.cotravellers_list=response.data;
       console.log(this.cotravellers_list);
     

     })*/


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartdrivePage');
  }
  
  goBack(){
   this.navCtrl.pop();
 }
 
 location() {
  this.navCtrl.push('LocationPage',{'data':this.cotravellers_list,'ride':this.data});
}
chat(id){
    
  var chatdata={'From_id':this.userdata.id,
                'To_id':id}
   this.navCtrl.push('Chat',{'data':chatdata,'userdata':this.userdata});
}
phone(number){
  this.num=JSON.stringify(number);
  console.log(this.num);
  this.callNumber.callNumber(this.num, true)
  .then(() => console.log('Launched dialer!'))
  .catch(() => console.log('Error launching dialer'));

}
 time_cal(req_time,res_time){
     var req_array  = req_time.split(':');
     var res_array  = res_time.split(':');
     var hour = parseInt(req_array[0])+parseInt(res_array[0]);
     var min = parseInt(req_array[1])+parseInt(res_array[1]);
     if(min>=60){
       hour = hour+1;
       min = min-60;
     }

     if(hour>=24){
       hour = hour-24;
     }
     var time_min = min>10?min:"0"+min;
     return hour+":"+time_min+":00";
   }

   set_hours(time_val){
     var time_array  = time_val.split(':');
     var hour_val = 'Am';
     if(time_array[0]>12){
       hour_val = 'Pm';
       time_array[0] = time_array[0] - 12;
       time_array[0] = time_array[0]>=10?time_array[0]:"0"+time_array[0];
     }
     return time_array[0]+":"+time_array[1]+" "+hour_val;
   }

   get_join(join_date){
     var d = new Date(join_date);
     var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
     return formattedDate;
   }
    get_loc(loc){
    var y = loc.split(",");
    return y[0];
  }
  get_left_loc(loc){
    let toArray = loc.split(",");
    toArray.splice (0, 2);
    return toArray.toString();
  }

}
