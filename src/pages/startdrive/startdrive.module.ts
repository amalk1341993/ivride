import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartdrivePage } from './startdrive';

@NgModule({
  declarations: [
    StartdrivePage,
  ],
  imports: [
    IonicPageModule.forChild(StartdrivePage),
  ],
  exports: [
    StartdrivePage
  ]
})
export class StartdrivePageModule {}
