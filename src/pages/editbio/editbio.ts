import { Component, ViewChild } from '@angular/core';
import { IonicPage,NavController,ViewController,ModalController,ToastController } from 'ionic-angular';
import { Nav, Platform,Events,MenuController } from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {FirebaseService} from '../../providers/firebaseservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@IonicPage()
@Component({
  selector: 'page-editbio',
  templateUrl: 'editbio.html'
})
export class Editbio {
  editbio = {userid :'',biography: ''};
   rootdb:any;
   rootref:any;
   attendees:any;
    currentUser: any;
  userdata: any;
  uid:any;
  constructor( public platform: Platform,
                public statusBar: StatusBar, 
                public splashScreen: SplashScreen,
                public firebaseService:FirebaseService,
                public afAuth: AngularFireAuth,  
                public events: Events,
                public menuCtrl: MenuController,
                public modalCtrl:ModalController,
                public viewCtrl:ViewController,
                private formBuilder: FormBuilder,
                private myservice: Myservice,
                private toastCtrl: ToastController
				) {}

  ionViewDidLoad() {
    var This = this;
    this.rootdb  = firebase.database().ref();
    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
      if (user) {
        var users = this.firebaseService.getCurrentUser();
        this.uid = users.uid;
        console.log(users.uid);
        this.rootdb.child('users').child(users.uid).on('value',function(snap){
          This.currentUser = snap.val();
          console.log(This.currentUser);
          if(This.currentUser.biography !== undefined && This.currentUser.biography !== ""){
            if(This.currentUser){
              This.userdata=This.currentUser;
              This.editbio.userid = This.userdata.id;
              This.editbio.biography = This.userdata.biography;
            }
          }
        })
      }
    });
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }
 
 update_bio(form) {
     var data = {"biography":this.editbio.biography};
     var res = this.rootdb.child('users').child(this.uid).update(data);
     if(res){
      let toast = this.toastCtrl.create({
          message: 'Successfully Updated',
          duration: 2000,
          position: 'bottom'
        });
          toast.present();
      }
      this.viewCtrl.dismiss();
 }

}
