import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Editbio } from './editbio';

@NgModule({
  declarations: [
    Editbio,
  ],
  imports: [
    IonicPageModule.forChild(Editbio),
  ],
  exports: [
    Editbio
  ]
})
export class EditbioModule {}
