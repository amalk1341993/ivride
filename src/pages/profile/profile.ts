import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams } from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class Profile {
 

tab:any;
 rootdb:any;
 data:any;
 currentUser: any;
 uid:any;
 email:any;
 usercar:any;
   profile_pic: any;
   profile_photo:any;
   email_is_verified:any;
   id:any;
  public base64Image : string;
  constructor(
              public popoverCtrl: PopoverController,
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              public storage: Storage, 
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public navParams: NavParams
    ) {
    this.id = this.navParams.get('id');
    this.currentUser = {"email":"","first_name":"","last_name":""};



         var This = this;
        this.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
           if (users.emailVerified === true) {
                 //console.log("verified");
                 this.email_is_verified=1;
               }
          This.uid = users.uid;
          this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.currentUser = snap.val();
            if(This.currentUser.profile_photo!=null && This.currentUser.profile_photo!='' ){
              This.profile_photo = This.currentUser.profile_photo;
            }
          })

          this.rootdb.child('car_details').on('value',function(snap){
           This.usercar = snap.val(); //console.log(This.usercar);
              if(This.usercar!=null && This.usercar!='' ){
                   var new_array = new Array();
                  for(var i in This.usercar){
                    if(This.usercar[i].user_id==users.uid){
                    new_array.push(This.usercar[i]);
                   }
                  }
                  This.usercar = new_array;
            }
          })
        }
        });
  }

  ionViewDidLoad() {
  this.tab = "active";

  }
  goBack(){
     this.navCtrl.pop();
   }

   tab_swap(type) {
       this.tab = type;
      }

      presentPopover(myEvent) {
  let popover = this.popoverCtrl.create('Popover');
  popover.present({
    ev: myEvent
  });
}

editprofile() {
let modal = this.modalCtrl.create('Editprofile');
modal.present();
}

editphoto() {
let modal = this.modalCtrl.create('Editphoto');
modal.present();
}

editbio() {
let modal = this.modalCtrl.create('Editbio');
modal.present();
}


editpreference() {
let modal = this.modalCtrl.create('Editpreference');
modal.present();
}

editid() {
let modal = this.modalCtrl.create('Editid');
modal.present();
}

addnumber() {
let modal = this.modalCtrl.create('Addnumber');
modal.present();
}

addcar() {
let modal = this.modalCtrl.create('Addcar');
modal.present();
}

addcarphoto(id) {
  console.log(id);
let modal = this.modalCtrl.create('Addcarphoto',{id:id});
modal.present();
}

editcar(id) {
  //console.log(id);
let modal = this.modalCtrl.create('Editcar',{id:id});
modal.present();
}

publicprofile() {
let modal = this.modalCtrl.create('Publicprofile');
modal.present();
}

notification() {
let modal = this.modalCtrl.create('Notification');
modal.present();
}

message() {
this.navCtrl.push('Message');
}

changePassword() {
let modal = this.modalCtrl.create('Password');
modal.present();
}
  help() {
  let modal = this.modalCtrl.create('HelpPage');
  modal.present();
  }

  terms() {
  let modal = this.modalCtrl.create('TermsPage');
  modal.present();
  }

  license() { 
  let modal = this.modalCtrl.create('LicensePage');
  modal.present();
  }

  privacy() {
  let modal = this.modalCtrl.create('PrivacyPage');
  modal.present();
  }

}
