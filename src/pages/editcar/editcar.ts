import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,ViewController,NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-editcar',
  templateUrl: 'editcar.html'
})
export class Editcar {
rootdb:any;
 data:any;
 currentUser: any;
 uid:any;
  count:any;
  cars:any;
  year = [];
  colours:any[];
  car_type:any[];
  car_make:any[];
  length:any;
  id:any;

  add_car = {user_id: '', car_make:'',car_type:'',car_year:'',car_color:'',no_of_seats:''};
  constructor(    public popoverCtrl: PopoverController,
	              public modalCtrl: ModalController,
	              public navCtrl: NavController,
	              private toastCtrl: ToastController,
	              private myservice: Myservice,
	              public storage: Storage, 
	              private formBuilder: FormBuilder,
	              public firebaseService:FirebaseService,
	              public afAuth: AngularFireAuth,
	              public viewCtrl: ViewController,
	              public navParams: NavParams
              ) {
  	 this.id = this.navParams.get('id');console.log(this.id);
  	            	this.count=4;
               	var currentYear = new Date().getFullYear();
					for (var j = currentYear; j > currentYear-50; j--) {
				        this.year.push(j);
				    }

				  	var This = this;
				    this.rootdb  = firebase.database().ref();
				    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
				    if (user) {
				      var users = this.firebaseService.getCurrentUser();
				      this.add_car.user_id = users.uid;

				         this.rootdb.child('colors').on('value',function(snap){
				          var colours = snap.val();
                  var new_array = new Array();
                  for(var i in colours){
                    new_array.push(colours[i]);
                  }
                  This.colours = new_array;
                  //console.log(This.colours);
				          
				        })
                  this.rootdb.child('car_type').on('value',function(snap){
                  var car_type = snap.val();
                  var new_array = new Array();
                  for(var i in car_type){
                    new_array.push(car_type[i]);
                  }
                  This.car_type = new_array;
                  //console.log(This.car_type);
                  
                })
                  this.rootdb.child('car_make').on('value',function(snap){
                  var car_make = snap.val();
                  var new_array = new Array();
                  for(var i in car_make){
                    new_array.push(car_make[i]);
                  }
                  This.car_make = new_array;
                  //console.log(This.car_make);
                  
                })

				      
				        
				    }
				    });
  }

  ionViewDidLoad() {
  	var This=this;
  	
       
       This.rootdb.child('car_details').child(This.id).on('value',function(snap){
            This.add_car = snap.val();
            if(This.add_car!=null){
              This.add_car.car_make = This.add_car.car_type;
              This.add_car.car_type = This.add_car.car_type;
              This.add_car.car_year = This.add_car.car_year;
              This.add_car.car_color = This.add_car.car_color;
              This.count = This.add_car.no_of_seats;
            }
          })





  }
  incr_count() {
	  if(this.count < 9)
	  this.count++;
	console.log(this.count++);
  }
    dcr_count() {
	  if(this.count > 1){
	  this.count--;
	console.log(this.count--);
	}else {
		this.count==0;
	}
  }
  dismiss() {
   this.viewCtrl.dismiss();
 }
   edit_car() {
     var data = {  
             		"car_make":this.add_car.car_make,
             		"car_type":this.add_car.car_type,
             		"car_year":this.add_car.car_year,
        		    "car_color":this.add_car.car_color,
        			"no_of_seats":this.count,
                    "user_id":this.add_car.user_id
 				};
       var res = this.rootdb.child('car_details').child(this.id).update(data);
      let toast = this.toastCtrl.create({
      message: 'Car Successfully Added',
      duration: 2000,
      position: 'bottom'
      });
      toast.present();
      this.viewCtrl.dismiss();     
   }

}
