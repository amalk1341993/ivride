import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Editcar } from './editcar';

@NgModule({
  declarations: [
    Editcar,
  ],
  imports: [
    IonicPageModule.forChild(Editcar),
  ],
  exports: [
    Editcar
  ]
})
export class EditcarModule {}
