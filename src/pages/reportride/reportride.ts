import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportridePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-reportride',
  templateUrl: 'reportride.html',
})
export class ReportridePage {

  data1:any;
  data:any;
 

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
      //  this.modalvalue=navParams.get("data");
       this.data1=navParams.get("data");
       
      
  }

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss(this.data);
 }


}
