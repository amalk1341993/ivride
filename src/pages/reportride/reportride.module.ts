import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportridePage } from './reportride';

@NgModule({
  declarations: [
    ReportridePage,
  ],
  imports: [
    IonicPageModule.forChild(ReportridePage),
  ],
  exports: [
    ReportridePage
  ]
})
export class ReportridePageModule {}
