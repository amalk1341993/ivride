import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'ReadPage';
  tab2Root = 'AnswerPage';
  tab3Root = 'NotificationPage';
  tab4Root = 'ProfilePage';

  constructor() {

  }
}
