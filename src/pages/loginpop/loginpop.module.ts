import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Loginpop } from './loginpop';

@NgModule({
  declarations: [
    Loginpop,
  ],
  imports: [
    IonicPageModule.forChild(Loginpop),
  ],
  exports: [
    Loginpop
  ]
})
export class LoginpopModule {}
