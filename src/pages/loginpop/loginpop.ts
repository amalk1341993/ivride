import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import * as firebase from 'firebase/app';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-loginpop',
  templateUrl: 'loginpop.html'
})
export class Loginpop {
  login_status = true;
  private login : FormGroup;  
  private signup : FormGroup;
  email:any;
  password:any;
  mobile:any;
  user:any;
  first_name:any;
  last_name:any;
  biography:any;
  rootdb:any;

  constructor(public navCtrl: NavController,public viewCtrl: ViewController,private toastCtrl: ToastController,private formBuilder: FormBuilder,private afAuth: AngularFireAuth,public firebaseService:FirebaseService,) {
        this.login = this.formBuilder.group({ 
          email: ['', Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])],
          password: ['', Validators.required]
        });

        this.signup = this.formBuilder.group({
                    mobile:['', Validators.compose([
                                 Validators.minLength(8),
                                 Validators.maxLength(15),
                                 Validators.pattern('[0-9]+$'),
                                 Validators.required 
                               ])],
                    email: ['', Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])],
                    password: ['', Validators.required],
                   first_name: ['', Validators.required],
                   last_name: ['', Validators.required]
               });
        this.login_status = true;
  }

  ionViewDidLoad() {
  }
  dismiss() {
   this.viewCtrl.dismiss();
 }

 signUp(){
   this.login_status = false;
 }

 signin(){
   this.login_status = true;
 }

 home(){
   var This=this;
   this.email= this.login.value.email; console.log(this.email);
   this.password= this.login.value.password;
   firebase.auth().signInWithEmailAndPassword(this.email,this.password).then(result => {
          console.log(result);
           var current_user = This.afAuth.auth.currentUser;console.log(current_user);
          //this.myservice.show_loader();
          //this.navCtrl.push('Home'); 
          This.dismiss();
      }).catch(function(error) {
     console.log(error.code);
     console.log(error.message);
     let toast = This.toastCtrl.create({
      message: 'Invalid Credential',
      duration: 2000,
      position: 'bottom'
    });
      toast.present();
    This.login.reset();
    
  });
  }


signupForm() {
var This = this;
this.rootdb  = firebase.database().ref();
this.rootdb.child('users').orderByChild('email').equalTo(this.signup.value.email).once('value',function(snap){
   
   if(snap.exists()!=true){
    This.afAuth.auth.createUserWithEmailAndPassword(
      This.signup.value.email,
      This.signup.value.password
    ).then((success) => {
      This.afAuth.auth.onAuthStateChanged((user) => { // getting new user
        if (user) {
          if(user.emailVerified === false){
         user.sendEmailVerification().then(function(){
          console.log("email verification sent to mail");
        });
          }
          var today = new Date();console.log(today);
          //var This = this;
              var userObj ={   
                user_id: user.uid,                          //Create a user object
                first_name:This.signup.value.first_name,
                last_name:This.signup.value.last_name,
                email: This.signup.value.email,
                mobile: This.signup.value.mobile,
        join_date:today,
                createdAt: firebase.database.ServerValue.TIMESTAMP
              };
              This.firebaseService.registerUser(user.uid,userObj).then(()=>{ // update user to db
              This.dismiss();
              })
           }
      });
    });  
}
else{
      let toast = This.toastCtrl.create({
        message: 'Email Already Exist',
        duration: 2000,
        position: 'bottom'
      });
        toast.present();


}
    })
}


     manage() {
     this.navCtrl.push('Manage');
     }

}
