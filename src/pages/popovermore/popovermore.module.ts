import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Popovermore } from './popovermore';

@NgModule({
  declarations: [
    Popovermore,
  ],
  imports: [
    IonicPageModule.forChild(Popovermore),
  ],
  exports: [
    Popovermore
  ]
})
export class PopovermoreModule {}
