import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-popovermore',
  templateUrl: 'popovermore.html'
})
export class Popovermore {
  data:any;
  owner:any;
  book_id:any;
  type:any;
  ridestart=true;
  // cotravellerslist=true;

  constructor( public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
		    ) {
    this.data=navParams.get("data");
     this.owner=navParams.get("owner");
       this.book_id=navParams.get("book_id");
        this.type=navParams.get("type");
        if(this.data.status==1){
          this.ridestart=false;
           // this.cotravellerslist=false;
        }else{
           this.ridestart=true;
            // this.cotravellerslist=true;
        }
        console.log(this.type);
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('Hello Popovermore Page');
  }
  Ratethis(){
    
    let modal = this.modalCtrl.create('RattingmodalPage',{'data':this.data,'owner':this.owner,'book_id': this.book_id});
   modal.present();
    
     modal.onDidDismiss(data => {
      //this.nav.setRoot('Yourride');
      console.log(data);
     if(data){
      this.viewCtrl.dismiss({'rate':data});
     }
     
    //  this.navCtrl.push('Yourride');
  
  });
  



}
editoffer(){
  this.viewCtrl.dismiss();
  this.navCtrl.push('OffereditPage',{'data': this.data});


}
   deleteoffer(){
      //this.myservice.show_loader();
       /*this.myservice.load_post(this.data.id,"delete_offer_ride").subscribe(response => {
	  	//this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {

            this.myservice.show_alert("Success!", 'Delete successfully');
            setTimeout(() =>{
              this.navCtrl.setRoot('Yourride',this.navCtrl.getActive().component);
            },1000)
            
          }
       
      })*/

   }
activerideoffer(){
     // this.myservice.show_loader();
      /*this.myservice.load_post(this.data.id,"ridestart_push").subscribe(response => {
	  	//this.myservice.hide_loader();
      console.log(response);
          if(response.recipients) {
               this.ridestart=true;
                //  this.myservice.show_alert("", response.message);
               this.viewCtrl.dismiss();
            // this.cotravellerslist=true;
            // this.navCtrl.push('Yourride');
         
            
          }else{
              this.ridestart=true;
               this.viewCtrl.dismiss();
          }
       
      })*/

}
seecotravellers(){
 this.viewCtrl.dismiss();
  let modal = this.modalCtrl.create('CotravellersPage',{'data': this.data});
  modal.present();
  modal.onDidDismiss(data => {
    //this.nav.setRoot('Yourride');
    this.navCtrl.setRoot('Yourride',this.navCtrl.getActive().component);
  });
  // this.navCtrl.push('CotravellersPage',{'data': this.data});

}


}
