import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addcar } from './addcar';

@NgModule({
  declarations: [
    Addcar,
  ],
  imports: [
    IonicPageModule.forChild(Addcar),
  ],
  exports: [
    Addcar
  ]
})
export class AddcarModule {}
