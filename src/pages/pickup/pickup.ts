import { Component,ViewChild, ElementRef  } from '@angular/core';
import { IonicPage,NavController,ViewController,ModalController } from 'ionic-angular';
 
declare var google;
@IonicPage()
@Component({
  selector: 'page-pickup',
  templateUrl: 'pickup.html'
})
export class Pickup {
@ViewChild('map') mapElement: ElementRef;
map:any;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController,public modalCtrl: ModalController) {}

  ionViewDidLoad() {
      this.loadMap();
  }

  dismiss() {
   this.viewCtrl.dismiss();
   }


      pickex() {
      let modal = this.modalCtrl.create('Pickex');
      modal.present();
      }

 loadMap(){
	 var This = this;
	 var image_file = 'assets/img/marker1.png';
	 let latLng = new google.maps.LatLng('18.1124','79.0193');
	 let mapOptions = {
	  center: latLng,
	  zoom: 4,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

			
 }
}
