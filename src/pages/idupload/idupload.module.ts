import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Idupload } from './idupload';

@NgModule({
  declarations: [
    Idupload,
  ],
  imports: [
    IonicPageModule.forChild(Idupload),
  ],
  exports: [
    Idupload
  ]
})
export class IduploadModule {}
