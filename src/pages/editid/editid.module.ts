import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Editid } from './editid';

@NgModule({
  declarations: [
    Editid,
  ],
  imports: [
    IonicPageModule.forChild(Editid),
  ],
  exports: [
    Editid
  ]
})
export class EditidModule {}
