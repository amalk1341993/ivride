import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,PopoverController,ModalController } from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import {FirebaseService} from '../../providers/firebaseservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@IonicPage()
@Component({
  selector: 'page-editid',
  templateUrl: 'editid.html'
})
export class Editid {
   userdata:any;
  user_id:any;
  result:any;
  pancard_status:any;
  passpot_status:any;
  aadhar_status:any;
  rootdb:any;
  uid:any;
  currentUser:any;
  verification:any;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController,private myservice: Myservice,
   public popoverCtrl: PopoverController,public modalCtrl: ModalController,
   public firebaseService:FirebaseService,
   public afAuth: AngularFireAuth) {
        this.pancard_status = 0;
        this.passpot_status = 0;
        this.aadhar_status = 0;
  }

ionViewDidLoad() {
         var This = this;
        this.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
          This.uid = users.uid;
          this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.currentUser = snap.val();
          })
           This.rootdb.child('verification').child(This.uid).on('value',function(snap){
           This.verification = snap.val(); console.log(This.verification);
           if(This.verification){
                  this.pancard_status =  This.verification.pancard.status;
                 this.passpot_status =This.verification.passport.status;
                 this.aadhar_status = This.verification.adhar.status;
               }
          })
        }
    })
 }
  

  idupload() {
  let modal = this.modalCtrl.create('AdharuploadPage');
  modal.present();
  this.viewCtrl.dismiss();
  }

  panidupload() {
  let modal = this.modalCtrl.create('PancarduploadPage');
  modal.present();
  this.viewCtrl.dismiss();
  }

  passportidupload() {
  let modal = this.modalCtrl.create('PassportuploadPage');
  modal.present();
  this.viewCtrl.dismiss();
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }

}
