import { Component } from '@angular/core';
import { IonicPage,NavController,ModalController,ToastController,ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@IonicPage()
@Component({
  selector: 'page-password',
  templateUrl: 'password.html'
})
export class Password {
private changePassword : FormGroup;
 currentUser: any;
 rootdb:any;
 email:any;
 password_error:any;
 password_success:any;
 pswrd = {userid:'',currentPassword:'',newPassword:'',confirmpassword:''};
  constructor( public navCtrl: NavController,public modalCtrl: ModalController,
               private toastCtrl: ToastController,
               private myservice: Myservice,
               public storage: Storage, 
               private formBuilder: FormBuilder,
               public firebaseService:FirebaseService,
               public afAuth: AngularFireAuth,
                public viewCtrl:ViewController
               ) {
  		          this.changePassword = this.formBuilder.group({
              	 	 currentPassword: ['', Validators.required],
              	 	 newPassword: ['', Validators.required],
              	 	 confirmpassword: ['', Validators.required]
	             });
  }

  ionViewDidLoad() {
  	    var This = this;
    this.rootdb  = firebase.database().ref();
    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
      if (user) {
        var users = this.firebaseService.getCurrentUser();
        this.email = users.email;
        console.log(users.uid);
        this.rootdb.child('users').child(users.uid).on('value',function(snap){
          This.currentUser = snap.val();
          console.log(This.currentUser);
        })
      }
    });
  }

	changePasswordForm(){
		if(this.pswrd.newPassword != this.pswrd.confirmpassword) {
			this.myservice.show_alert("Error!", "Password doesn't match");
		}
		else {
			var This = this;
			var user = firebase.auth().currentUser;
			this.email = user.email;
			this.afAuth.auth.signInWithEmailAndPassword(
				this.email,
				this.pswrd.currentPassword,
			).then((success) => {
				console.log("logged");
				var user = firebase.auth().currentUser;
				user.updatePassword(this.pswrd.newPassword).then(function() {
					This.password_success = true;
					setTimeout(function(){
					This.password_success = false;
				},3000);
				let toast = This.toastCtrl.create({
					message: 'Successfully Updated',
					duration: 2000,
					position: 'bottom'
				});
				toast.present();
				This.pswrd.newPassword = '';
				This.pswrd.currentPassword = '';
				This.pswrd.confirmpassword = '';
				This.viewCtrl.dismiss();
				}).catch(function(error) {   
					console.log(error); 
					This.password_success = true;
					setTimeout(function(){
					This.password_success = false;
					},3000);
				});
			}).catch(function(error) { 
				This.password_error = true;
				setTimeout(function(){
					This.password_error = false;
				},3000);
				This.myservice.show_alert("Error!", "Current Password is wrong..!");
			});
		}
	}
  dismiss() {
   this.viewCtrl.dismiss();
 }

}
