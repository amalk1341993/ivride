import { Component } from '@angular/core';
import { IonicPage,NavController,ModalController,ViewController,ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@IonicPage()
@Component({
  selector: 'page-editpreference',
  templateUrl: 'editpreference.html'
})
export class Editpreference {
  userdata:any;
  rootdb:any;
  data:any;
  currentUser: any;
  uid:any;
  preference:any;
  user_id:any;s
  edit_pre = {userid: '', chattiness:'',smoking:'',music:'',pets:''};
  constructor(
			  public navCtrl: NavController,
			  public modalCtrl: ModalController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              public storage: Storage, 
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public viewCtrl:ViewController
  	) {}

  ionViewDidLoad() {
    var This = this;
    this.rootdb  = firebase.database().ref();
    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
      if (user) {
        var users = this.firebaseService.getCurrentUser();
        this.uid = users.uid;
        console.log(users.uid);
        this.rootdb.child('preference').child(users.uid).on('value',function(snap){
          This.currentUser = snap.val();
          console.log(This.currentUser);
          if(This.currentUser !== undefined && This.currentUser !== ""){
            if(This.currentUser){
              This.edit_pre=This.currentUser;
          
            }
          }
        })
      }
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  
  save_preferences() {
     var data = {
     	"user_id":this.uid,
     	"chattiness":this.edit_pre.chattiness,
     	"smoking":this.edit_pre.smoking,
     	"music":this.edit_pre.music,
     	"pets":this.edit_pre.pets
     };
     var res = this.rootdb.child('preference').child(this.uid).update(data);
     if(res){
      let toast = this.toastCtrl.create({
          message: 'Successfully Updated',
          duration: 2000,
          position: 'bottom'
        });
          toast.present();
      }
      this.viewCtrl.dismiss();
  }

}

