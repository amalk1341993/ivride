import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Editpreference } from './editpreference';

@NgModule({
  declarations: [
    Editpreference,
  ],
  imports: [
    IonicPageModule.forChild(Editpreference),
  ],
  exports: [
    Editpreference
  ]
})
export class EditpreferenceModule {}
