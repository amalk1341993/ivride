import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html'
})
export class Filter {
   filterride:FormGroup;
seats:any;
today:any;
date:any;
month:any;
tom:any;
month2:any;
dep_date = false;
ride_date = false;
seat_number= false;
  constructor(	 public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController,
                  private formBuilder: FormBuilder
			) {
        this.filterride = formBuilder.group({
      'ride_date' : ['', Validators.required],
      'ride_time':['', Validators.required],
     
      
      
    }); 
    this.today=new Date();
    this.date = this.today.getFullYear()+'-'+(this.today.getMonth()+1)+'-'+this.today.getDate();
    var myDate = new Date();
    var nextDay = new Date(myDate);
    nextDay.setDate(myDate.getDate()+1);
    this.tom = nextDay.toISOString();
    this.month=this.tom ;
  }

  ionViewDidLoad() {
  }
    // public event = {
    //   month: '2016-11-01',
    //   timeStarts: '07:43',
    //   timeEnds: '2050-02-20'
    // }
      seatFn(event){
      var target = event.target 
      event.srcElement;
      this.seats = event.target.innerHTML;
      console.log(this.seats);

  }
  filterrideForm(event,post){
    
    var departure_date =post.ride_date.split("T");
    post.ride_date= departure_date[0];
    var error = 0;
    if(post.ride_date=='' || post.ride_date==null){
      this.dep_date = true;
      error = 1;
    } else {
      this.dep_date = false;
      error = 0;
    }

    if(post.ride_time=='' || post.ride_time==null){
      this.ride_date = true;
      error = 1;
    } else {
      this.ride_date = false;
      error = error!=1?0:1;
    }

    if(this.seats=='' || this.seats==null){
      this.seat_number= true;
      error = 1;
    } else {
      this.seat_number= false;
      error = error!=1?0:1;
    }
     if(error==0){
       var data={
        "travel_date":post.ride_date,
        "travel_time":post.ride_time,
        "seats":this.seats
      }
      this.viewCtrl.dismiss(data);
    } else {

    }
    

  }

  dismiss() {
   
   this.viewCtrl.dismiss();
 }

}
