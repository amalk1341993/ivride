import { Component,ViewChild, ElementRef } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
declare var google;
//@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class Locations {
	
	@ViewChild('map') mapElement: ElementRef;
  map: any;
  data:any;
  place:any;
  markers:any;
  latLng:any;
  lattitude:any;
  longitude:any;
  data1:any;
  key_num:any;
  ride_details:any;
rootdb:any;
userdata:any;
  constructor(
                 public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
              ) {
    console.log("i'm here");
     this.data1=navParams.get("data");
     this.ride_details =navParams.get("ride");
     console.log(this.ride_details);




           var This = this;
        This.rootdb  = firebase.database().ref();
        var users = This.firebaseService.getCurrentUser();//console.log(users.uid);
        this.userdata = users.uid;

        this.rootdb.child('users').child(this.ride_details.driver_id).on('value',function(snap){
          This.data1 = snap.val();
        })



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
	this.loadMap();
  }
  
   goBack(){
   this.navCtrl.pop();
 }

 
 loadMap(){
   var This = this;
    var directionsDisplay = new google.maps.DirectionsRenderer;
     var directionsService = new google.maps.DirectionsService;
    
    let latLng = new google.maps.LatLng(10.0139496,76.3635918);
    console.log(latLng);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      directionsDisplay.setMap(this.map);
      this.calculateAndDisplayRoute(directionsService, directionsDisplay);
      //this.data1.forEach((element,key) => { 
      this.lattitude= This.data1.latitude;//element.crnt_lattitude;
      this.longitude= This.data1.longitude; //element.crnt_longitude;
      this.key_num= 1;
      this.addmarker(this.lattitude,this.longitude,this.key_num);
      	//});

 
  }
  calculateAndDisplayRoute(directionsService,directionsDisplay){
     var selectedMode ="DRIVING";
        directionsService.route({
          origin: {lat:Number(this.ride_details.source_lat), lng:Number(this.ride_details.source_lng)},  // Haight.
          destination: {lat:Number(this.ride_details.destination_lat), lng:Number(this.ride_details.destination_lng)},  // 
  
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            this.loadMap();
            console.log("called zero result")
            console.log('Directions request failed due to ' + status);
          }
        });
  }
  

  addmarker(lat,long,key){
    console.log("add")
    	var icon = {
			url: "assets/img/marker3.png", // url
			scaledSize: new google.maps.Size(30, 30), // scaled size
			origin: new google.maps.Point(0,0), // origin
			anchor: new google.maps.Point(0, 0) // anchor
		};

     this.markers= new google.maps.Marker({
     animation: google.maps.Animation.DROP,
		 position: new google.maps.LatLng(lat,long),
     icon: icon,
      label: {
                text: JSON.stringify(key),
                color: 'white',
  },
      // label:JSON.stringify(key) ,
     
     description: "IVERide"
     
     
		});
  this.markers.setMap(this.map);
  // })

}

  

  
  pay() {


    console.log(this.ride_details);
     //var data_user={'data':this.data1,'ride':this.ride_details};
    // this.myservice.show_loader();

 var data={'ride_id':this.data.id,
               'user_id':this.userdata,
               'driver_id':this.data.users.user_id,
               'status':2
              }
 this.rootdb.child('ride_status').child(this.userdata).update(data);
 this.navCtrl.push('Yourride');

/*    this.myservice.load_post(this.ride_details,"end_ride_update").subscribe(response => {
	  	this.myservice.hide_loader();
      console.log(response);
          if(response.status == "success") {
         
          	this.navCtrl.push('PayPage',{'data':this.data1,'ride':this.ride_details});
          }
       
      })*/





}

}
