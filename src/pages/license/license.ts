import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,ViewController } from 'ionic-angular';
/**
 * Generated class for the LicensePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-license',
  templateUrl: 'license.html',
})
export class LicensePage {
rootdb:any;
userlicense:any;
  constructor(	public navCtrl: NavController, 
	  		    public modalCtrl: ModalController,
	            private toastCtrl: ToastController,
	            private myservice: Myservice,
	            public storage: Storage, 
	            private formBuilder: FormBuilder,
	            public firebaseService:FirebaseService,
	            public afAuth: AngularFireAuth,
	            public viewCtrl: ViewController
  	) 
  {}

  ionViewDidLoad() {
     console.log('ionViewDidLoad HelpPage');
     var This=this;
      This.rootdb  = firebase.database().ref();
      This.rootdb.child('cms').child('licenses').once('value',function(snap){
      //console.log(snap.val());
       This.userlicense = snap.val();

        })
  }
  dismiss() {
   this.viewCtrl.dismiss();
 }
}
