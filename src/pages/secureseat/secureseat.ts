import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-secureseat',
  templateUrl: 'secureseat.html'
})
export class Secureseat {
data:any;
source_sp:any;
source_head:any;
source_new:any;
destination_sp:any;
destination_head:any;
destination_new:any;
seat:any;
seat_new:any;
bookid:any;
user_data:any;
newdata:any;
rate:any;
rootdb:any;
res:any;
ride_id:any;
date:any;
year:any;
datas:any;
ride_data:any;
  constructor(   public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
             ) {
        this.data  = this.navParams.get("data");
        this.ride_id = this.navParams.get('ride_id');
        this.seat = this.navParams.get('seat');
        this.rate = {ratenum:0};
        var user = this.firebaseService.getCurrentUser();
        var This = this;

        this.rootdb  = firebase.database().ref();

        this.rootdb.child('users').child(user.uid).once('value',function(snap){
          This.user_data = snap.val();
        })

        this.rootdb.child('ride').child(this.ride_id).once('value',function(snap){
          This.ride_data = snap.val();
        })

        console.log(this.user_data);

          /*this.ride_id = this.navParams.get("ride_id");
          this.seat = this.navParams.get("seat");
          this.rootdb  = firebase.database().ref();
          this.user_data = this.firebaseService.getCurrentUser();
          console.log(this.user_data);
          //this.user_data = users;
          var This = this;
          

          

          
          console.log(this.ride_id,this.seat);*/

          




          



     
   
   
    
    /*this.bookid =navParams.get("bookid");
    this.rate=navParams.get("rate");*/

    /*console.log(this.bookid);
    this.source_sp= this.data.source.split(',');
            this.source_head=this.source_sp[0];
            console.log(this.source_sp);           
              let string_val=this.data.source;
              let toArray = string_val.split(",");
              toArray.splice (0, 1);
              let str=toArray.toString();
              this.source_new=str;
            this.destination_sp= this.data.destination.split(',',2);
            this.destination_head=this.destination_sp[0];
             let string_val1=this.data.destination;
              let toArray1 = string_val1.split(",");
              toArray1.splice (0, 1);
              let str1=toArray.toString();
             

             this.destination_new= str1;
             console.log(this.data.no_of_seats)
             console.log( this.seat);
             console.log( this.data.no_of_seats - this.seat)
             console.log( this.data.no_of_seats - this.seat)
             this.seat_new = this.data.no_of_seats - this.seat;
             console.log(this.seat_new);*/
  }

  ionViewDidLoad() {
    /*var This = this;
    //if(this.ride_id!=undefined){
            
            this.rootdb.child('ride').child(This.ride_id).on('value',function(snap){
             This.data = snap.val();
             This.data.reached_time = This.time_cal(This.data.departure_time,This.data.approximatetime);
             This.data.departure_time = This.set_hours(This.data.departure_time);
             This.data.reached_time = This.set_hours(This.data.reached_time);

         This.rootdb.child('users').child(This.data.users_id).once('value',function(snap){
           //This.find_data.users = snap.val();
           var users = snap.val();
            This.date=new Date();
            This.year= This.date.getFullYear();
            if(users.dob){
              var dob = users.dob.split("-");
              users.age = This.year-dob[2]+" Y/O";
            } else {
              users.age = '';
            }
            This.data.users = users;
         })

        

         


         console.log(This.data);
       })
          //}*/
  }

  dismiss() {
   this.viewCtrl.dismiss();
   }

   time_cal(req_time,res_time){
     var req_array  = req_time.split(':');
     var res_array  = res_time.split(':');
     var hour = parseInt(req_array[0])+parseInt(res_array[0]);
     var min = parseInt(req_array[1])+parseInt(res_array[1]);
     if(min>=60){
       hour = hour+1;
       min = min-60;
     }

     if(hour>=24){
       hour = hour-24;
     }
     var time_min = min>10?min:"0"+min;
     return hour+":"+time_min+":00";
   }

   set_hours(time_val){
     var time_array  = time_val.split(':');
     var hour_val = 'Am';
     if(time_array[0]>12){
       hour_val = 'Pm';
       time_array[0] = time_array[0] - 12;
       time_array[0] = time_array[0]>=10?time_array[0]:"0"+time_array[0];
     }
     return time_array[0]+":"+time_array[1]+" "+hour_val;
   }


   get_loc(loc){
    var y = loc.split(",");
    return y[0];
  }

  get_left_loc(loc){
    let toArray = loc.split(",");
    toArray.splice (0, 1);
    return toArray.toString();
  }

   request() {
     console.log(this.ride_id);
     var This = this
       var user = this.firebaseService.getCurrentUser();
       var res = {
                  'user_id':user.uid,
                  'ride_id':this.ride_id,
                  'driver_id':This.ride_data.users_id,
                  'source':this.data.source,
                  'destination':this.data.destination,
                  'departure_date':this.data.departure_date,
                  'departure_time':this.data.departure_time,
                  'no_of_seat':this.seat,
                  'book_date':firebase.database.ServerValue.TIMESTAMP,
                  'amount':this.data.price*this.seat,
                  'car_id':this.data.car_id
               }

               console.log(res);

        var messageListRef = firebase.database().ref('booking');
        var newMessageRef = messageListRef.push(res);
        console.log(newMessageRef.key);
        this.rootdb.child('booking').child(newMessageRef.key).update({id:newMessageRef.key});
            let toast = this.toastCtrl.create({
              message: "Booked Successfully",
              duration: 2000,
              position: 'bottom'
            });
            toast.present();

            var message = This.user_data.first_name+" has been booked your "+This.seat+" seat(s) From: "+This.data.source+" TO: "+This.data.destination+" @"+this.data.departure_date+" "+this.data.departure_time;

            var post_data = {"id":This.ride_data.users_id}
            var push_data = {"user_id":This.ride_data.users_id,"sub":"ride booked","booked_user":user.uid,"message":message};
            //This.rootdb.child('notify_data').child(This.ride_data.users_id).push(push_data);
            This.myservice.load_post(post_data,"send_Message").subscribe(response => {
              var query = This.rootdb.child('notify_data').child(This.ride_data.users_id).push(push_data);
              var key = query.key;
              This.rootdb.child('notify_data').child(This.ride_data.users_id).child(key).update({"id":key});
            });

            var adaRef = firebase.database().ref('ride/' + this.ride_id + '/no_of_seats');
            adaRef.transaction(function(tagValue) {
                return tagValue ? tagValue - This.seat : 0;
            })
        //setTimeout(() =>{
            this.navCtrl.push('Yourride');


        //var id = newMessageRef.key;
        
               //console.log(id);
                //var data = this.firebaseService.setItem("booking",id,res); 
                //console.log();
       /* newMessageRef.set({
          
          'user_id':this.user_data.id,
          'ride_id':this.ride_id,
          'source':this.data.source,
          'destination':this.data.destination,
          'departure_date':this.data.departure_date,
          'departure_time':this.data.departure_time,
          'no_of_seat':this.seat,
          'book_date':firebase.database.ServerValue.TIMESTAMP,
          'amount':this.data.price*this.seat
        });*/




    //this.myservice.show_loader();
         /*var This = this;
         This.rootdb  = firebase.database().ref(); 
         This.rootdb.child('booking').orderByChild('id').equalTo(this.bookid).once('value',function(snap){
              this.booking = snap.val();
               This.rootdb.child('users').orderByChild('user_id').equalTo(this.booking.user_id).once('value',function(snap){
                  this.users = snap.val();
              })
               This.rootdb.child('ride').orderByChild('id').equalTo(this.booking.ride_id).once('value',function(snap){
                  this.rides = snap.val();
              })
               var bookpush={"user_id":this.users.user_id,"book_id":this.booking.id}
                var res = this.firebaseService.pushItem("book_push",bookpush); 
                console.log(res); 
                var dat={car_id:res}
                this.rootdb.child('book_push').child(res).update(dat);
          })



    var content = {
      "en" : 'New ride alert'
      }
    var headings = {
              "en" : 'Ride'
              }

      var include_player_ids  = this.res;
      var type = 'booked';
      //$res= $this->send_Message(content,headings,include_player_ids,result,$type);
      




         this.navCtrl.push('Request',{'data': this.data,'rate':this.rate});*/

     



  
   }

}
