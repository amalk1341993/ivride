import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Secureseat } from './secureseat';

@NgModule({
  declarations: [
    Secureseat,
  ],
  imports: [
    IonicPageModule.forChild(Secureseat),
  ],
  exports: [
    Secureseat
  ]
})
export class SecureseatModule {}
