import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-thanks',
  templateUrl: 'thanks.html'
})
export class Thanks {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {}

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }

}
