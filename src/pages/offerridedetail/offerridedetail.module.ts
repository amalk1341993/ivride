import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Offerridedetail } from './offerridedetail';

@NgModule({
  declarations: [
    Offerridedetail,
  ],
  imports: [
    IonicPageModule.forChild(Offerridedetail),
  ],
  exports: [
    Offerridedetail
  ]
})
export class OfferridedetailModule {}
