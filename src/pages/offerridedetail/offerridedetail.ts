import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams,AlertController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-offerridedetail',
  templateUrl: 'offerridedetail.html'
})
export class Offerridedetail {
  place_from:any;
  placed_to:any;
  price:any;
  seats:any;
  detour:any;
  flexibility:any;
  luggage:any;
  userdata:any;
  comments:any;
  roundtripdata:any;
  check:any;
  user_id:any;
  privacy:any;
  car_name:any;
  cardetails_data:any;
  car_id:any;
test:any;
selected:any;
car_name_new:any;
data:any
passed_data:any;
// car_id_new:any;
rootdb:any;
 usercar:any;
 users:any;
 uid:any;
 currentUser: any;
 messenger:any;
 approximatetime:any;



  constructor(
                public popoverCtrl: PopoverController,
                public modalCtrl: ModalController,
                public navCtrl: NavController,
                private toastCtrl: ToastController,
                private myservice: Myservice,
                public storage: Storage, 
                private formBuilder: FormBuilder,
                public firebaseService:FirebaseService,
                public afAuth: AngularFireAuth,
                public navParams: NavParams,
                public alertCtrl :AlertController
              ) {
  this.place_from = navParams.get("firstPassed");
  this.placed_to = navParams.get("secondPassed");
  this.roundtripdata = navParams.get("thirdPassed");//console.log(this.roundtripdata);
   this.data = navParams.get("data");console.log(this.data);
   this.passed_data = navParams.get("data");console.log(this.passed_data);
  this.check = navParams.get("fourthPassed");//console.log(this.check);
  this.privacy=false;
  this.price = 50;
  this.seats= 1;
  this.luggage="Small";
  this.flexibility="Right on time";
  this.detour="15 Minutes Detour max.";
  this.approximatetime="00:00";

/*  this.storage.get('userdata').then((userdata) => {
     setTimeout(() =>{
        if(userdata){
     this.userdata=userdata;
       this.user_id=userdata.id;
      
    }else{
        this.userdata=userdata;
      this.user_id='0';
      
        

    } 
    this.car_details();
       },1000);
    
  })*/
  this.car_details();


  this.messenger ={
      message: (this.comments ? this.comments : '' ),
      rtrn_date: (this.data.rtrn_date ? this.data.rtrn_date : '' ),
      rtrn_time: (this.data.rtrn_time ? this.data.rtrn_time : '' ),
    };
console.log(this.messenger );

//this.messenger = JSON.parse(JSON.stringify(this.messenger));
//console.log(this.messenger );



}


  car_details(){
      var This=this;
      This.rootdb  = firebase.database().ref();
      var users = This.firebaseService.getCurrentUser();
      This.rootdb.child('car_details').on('value',function(snap){
        This.usercar = snap.val(); //console.log(This.usercar);
        if(This.usercar!=null && This.usercar!='' ){
          var new_array = new Array();
          for(var i in This.usercar){
            if(This.usercar[i].user_id==users.uid){
              new_array.push(This.usercar[i]);
            }
          }
          This.usercar = new_array;//console.log(This.usercar);
          //setTimeout(() =>{
          This.cardetails_data=This.usercar;
          This.car_id=This.usercar.car_id;   
        // },1000) 
        }
      })
  }

decrement(price1){
if(this.price>10){
  this.price=  this.price-5;
   //console.log(this.price);

}
 

}
increment(price){
  this.price=  this.price+5;
    // console.log(this.price);

}
incrementseats(seats){
   if(this.seats<5){
  this.seats=this.seats+1;
  }
  

}
decrementseats(seats){
  if(this.seats>1){
  this.seats=this.seats-1;
   } else {
     this.seats=1;
   }

}
check_privacy(){
  this.privacy=!this.privacy;
}

  ionViewDidLoad() {
    var This = this;
    This.rootdb  = firebase.database().ref();
    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
      if (user) {
        var users = This.firebaseService.getCurrentUser();
        This.uid = users.uid;
        console.log(users.uid);
        This.rootdb.child('users').child(users.uid).on('value',function(snap){
          This.currentUser = snap.val();
          //console.log(This.currentUser);

        })
      }
    });
  }

  addcomment() {
  let modal = this.modalCtrl.create('Addcomment');
  modal.present();
  modal.onDidDismiss(data => {
    this.comments=data;
    console.log(data);
  });
}

loginpop() {
     //console.log(this.car_id);
     //console.log(this.luggage);
            if(this.car_id== undefined){
               this.myservice.show_alert("Error!",'please select car');
            }
            else if(this.seats==0){
              this.myservice.show_alert("Error",'select Atleast one seat');
      
            }else if(this.price==0){
              this.myservice.show_alert("Error",'Price should be greater than 0');
            }
            else{

              var departure_date = this.data.departure_date.split("T");
              this.data.departure_date = departure_date[0];
              if(this.data.round_trip){
                var rtrn_date = this.data.departure_date.split("T");
                this.data.rtrn_date = rtrn_date[0];
              }

var array1= {
             'users_id':this.uid,
             "source":this.data.source,
             "source_lat":this.data.source_lat,   
             "source_lng":this.data.source_lng,   
             "destination":this.data.destination,
             "destination_lat":this.data.destination_lat,
             "destination_lng":this.data.destination_lng,
             "car_id":this.car_id,
             "price":this.price,
             "no_of_seats":this.seats,
             "sour_dest_date":this.data.source+"#"+this.data.destination+"#"+this.data.departure_date,
             'departure_date':this.data.departure_date,
             'departure_time':this.data.detour_time,
             "approximatetime":this.approximatetime,
             "max_luggage":this.luggage,
             "detour_time":this.detour,
             "pickup_flexibility":this.flexibility,
             "comments": (this.comments ? this.comments : undefined ),
             "status":''
         }

var array2 = {

             'users_id':this.uid,
             "destination":this.data.source,
             "destination_lat":this.data.source_lat,   
             "destination_lng":this.data.source_lng,   
             "source":this.data.destination,
             "source_lat":this.data.destination_lat,
             "source_lng":this.data.destination_lng,
             "car_id":this.car_id,
             "price":this.price,
             "no_of_seats":this.seats,
             "sour_dest_date":this.data.destination+"#"+this.data.source+"#"+this.data.departure_date,
             "max_luggage":this.luggage,
             "detour_time":this.detour,
             "pickup_flexibility":this.flexibility,
             "comments":(this.comments ? this.comments : undefined ),
              "approximatetime":this.approximatetime,
             "round_trip":this.data.round_trip,
             "rtrn_date":(this.data.rtrn_date ? this.data.rtrn_date : undefined ),
             "rtrn_time":(this.data.rtrn_time ? this.data.rtrn_time : undefined ),
              "status":''
            }

var array3 = JSON.parse(JSON.stringify(array1));
var array4 = JSON.parse(JSON.stringify(array2));



if(this.data.round_trip==''){
      var res = this.firebaseService.pushItem("ride",array3); 
      var dat={id:res}
      this.rootdb.child('ride').child(res).update(dat);
            let toast = this.toastCtrl.create({
              message: "Successfully Added",
              duration: 2000,
              position: 'bottom'
            });
              toast.present();
        //setTimeout(() =>{
            this.navCtrl.push('Offered');
          //},1000);
}
else{
      var res = this.firebaseService.pushItem("ride",array3); 
      var dat1={id:res}
      this.rootdb.child('ride').child(res).update(dat1);
      var res2 = this.firebaseService.pushItem("ride",array4); 
      var dat2={id:res2}
      this.rootdb.child('ride').child(res2).update(dat2);
      let toast = this.toastCtrl.create({
              message: "Successfully Added",
              duration: 2000,
              position: 'bottom'
            });
              toast.present();
        //setTimeout(() =>{
            this.navCtrl.push('Offered');
          //},1000);
    }
   
  }
}
addcar() {
let modal = this.modalCtrl.create('Addcar');
modal.present();
}
  goBack(){
     this.navCtrl.pop();
   }

}
