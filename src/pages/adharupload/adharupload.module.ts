import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdharuploadPage } from './adharupload';

@NgModule({
  declarations: [
    AdharuploadPage,
  ],
  imports: [
    IonicPageModule.forChild(AdharuploadPage),
  ],
  exports: [
    AdharuploadPage
  ]
})
export class AdharuploadPageModule {}
