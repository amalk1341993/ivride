import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';

@IonicPage()
@Component({
  selector: 'page-alert',
  templateUrl: 'alert.html'
})
export class Alert {
  data:any;
  source_sp:any;
  source_head:any;
  source_new:any;
  destination_sp:any;
  destination_head:any;
  destination_new:any;
   trip: any;
   user_data:any;
   month:any;
   today:any;
   date:any;
   time:any;
   before_time:any;
   tom:any;
rootdb:any;
  constructor(   public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
			) {
        
        var This = this;
        This.rootdb  = firebase.database().ref();
        var users = This.firebaseService.getCurrentUser();
        this.user_data=users;

    
    this.today=new Date();
    this.date = this.today.getFullYear()+'-'+(this.today.getMonth()+1)+'-'+this.today.getDate();
     this.data = navParams.get("data");
     console.log(this.data);
      this.source_sp= this.data.source.split(',');
            this.source_head=this.source_sp[0];
            console.log(this.source_sp);           
              let string_val=this.data.source;
              let toArray = string_val.split(",");
              toArray.splice (0, 1);
              let str=toArray.toString();
              this.source_new=str;
            this.destination_sp= this.data.destination.split(',',2);
            this.destination_head=this.destination_sp[0];
             let string_val1=this.data.destination;
              let toArray1 = string_val1.split(",");
              toArray1.splice (0, 1);
              let str1=toArray.toString();
             

             this.destination_new= str1;
             this.trip = { lower: 10, upper: 20 };
             console.log(this.trip);

             var myDate = new Date();
             var nextDay = new Date(myDate);
             nextDay.setDate(myDate.getDate()+1);
             this.tom = nextDay.toISOString();
             this.month=this.tom ;



  }
 
     
  // public event = {
  //     month: '2016-11-01',
  //     timeStarts: '07:43',
  //     timeEnds: '2050-02-20',
  //   }
    

  ionViewDidLoad() {
  }
  dismiss(){
     this.viewCtrl.dismiss();

  }

  goback() {
	  var users = this.firebaseService.getCurrentUser();
       console.log(event);
       var departure_date = this.month.split("T");
       this.month= departure_date[0];

    
      // this.myservice.show_loader();
    var ride_data={'user_id':users.uid,
                    'source':this.data.source,
                    'destination':this.data.destination,
                    'departure_date':this.month,                    
                    'departure_time':this.time,
                    'ride_alert':'1',
                    'sour_dest_date':this.data.source+'#'+this.data.destination+'#'+this.month
                    }
        console.log(ride_data);
	   this.rootdb.child('alert/').push(ride_data);
        this.myservice.show_alert("Success!", "Your request has been Created");
	  	//this.myservice.hide_loader();
      this.viewCtrl.dismiss();

   

 //this.myservice.hide_loader();
}

}
