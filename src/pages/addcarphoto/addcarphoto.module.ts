import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addcarphoto } from './addcarphoto';

@NgModule({
  declarations: [
    Addcarphoto,
  ],
  imports: [
    IonicPageModule.forChild(Addcarphoto),
  ],
  exports: [
    Addcarphoto
  ]
})
export class AddcarphotoModule {}
