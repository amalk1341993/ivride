import { Component } from '@angular/core';
import {IonicPage,NavController,ViewController,NavParams,Platform} from 'ionic-angular';
import {FirebaseService} from '../../providers/firebaseservice';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import * as firebase from 'firebase';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';
import {Myservice} from '../../providers/myservice';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class Chat {
data:any;
id1:any;
id2:any;
length:any;
final:any;
messages:any;
lastmessagekey:any;
msg:any;
userdata:any;
first_name:any;
locationmsg:any;
location:any;
scrolledmsg:any;
msg_show:any;
rootdb:any;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController,
  private iab: InAppBrowser,public navParams: NavParams,public firebaseService:FirebaseService,
  private platform: Platform, private geolocation: Geolocation,private myservice: Myservice
  ) {
     this.data= navParams.get("data");
    //  this.chatdata= navParams.get("chatdata");
    this.userdata= navParams.get("userdata");
    this.first_name =this.userdata.first_name;
    if(this.data){
       this.id1 = this.data.From_id;
       this.id2 = this.data.To_id;
    }
    
       console.log(this.data);
       this.msg_show =true;
       this.loadmsg();
platform.ready().then(() => {
             var This=this;
      geolocation.getCurrentPosition().then((location) => {
      
        console.log(location);
      
        This.location=location;
         console.log(this.location.coords.latitude);

        
      }).catch((error) => {
      
        console.log('Error getting location', error);
        
      });
       this.location=This.location;
      
    } );
       


  }
  loadmsg(){
  
      console.log(this.data);
      console.log(this.id1) ;   
       console.log(this.id2) ;
       var This = this;
                      //load last 10 messages initially//
if (this.id1 < this.id2) {

            //console.log(this.id1);
            /*var url = "chats/" + this.id1 + this.id2 + "/";
            var query = {
                query: {
                    limitToLast: 10
                }
            };
           
            This.rootdb  = firebase.database().ref();
            This.rootdb.child(url).limitToLast(10).on('value',function(snap){
            This.final = This.myservice.obj_array(snap.val());
          })*/


           /* this.length = this.firebaseService.listItem(url).subscribe((item) => {
                console.log('len',item.length);
                this.length = item.length;
            });*/






            This.rootdb  = firebase.database().ref();
            This.rootdb.child(url).limitToLast(10).on('value',function(snap){
            This.final = This.myservice.obj_array(snap.val());
            if(This.final.length!=0){
                This.messages = This.final;
                This.lastmessagekey = This.final[0].$key;
                console.log(this.lastmessagekey);
                }
            console.log(snap.val());
          })
        } else {
            var url = "chats/" + this.id2 +"-"+ this.id1 + "/";

            var query = {
                query: {
                    limitToLast: 10
                }
            };

            /*this.firebaseService.listItem(url).subscribe((item) => {
                console.log(item.length);
                this.length = item.length;
            });
*/


            This.rootdb  = firebase.database().ref();
            This.rootdb.child(url).limitToLast(10).on('value',function(snap){
            This.final = This.myservice.obj_array(snap.val());
            if(This.final.length!=0){
                This.messages = This.final;
                This.lastmessagekey = This.final[0].$key;
                console.log(This.lastmessagekey);
                }
            console.log(snap.val());
          })



           /* console.log(this.length);
            this.data = this.firebaseService.listItemQuery(url, query);

            console.log(this.data);


            this.data.subscribe((user) => {
                console.log(user);
                this.final = user;
                    if(user.length!=0){
                this.messages = user;
                this.lastmessagekey = user[0].$key;
                console.log(this.lastmessagekey);
                }

            })*/

        }

  }
  

  ionViewDidLoad() {
    console.log('Hello Chat Page');
  }
  send(){
      var msg = {
        "From_id": this.id1,
        "To_id": this.id2,
        "message": this.msg,
        "timestamp": firebase.database.ServerValue.TIMESTAMP,
        "type":'text'
       
      }
        console.log(msg);

        if (this.id1 < this.id2) {

        var url = "chats/" + this.id1 +"-"+this.id2 + "/";

        this.firebaseService.pushItem(url, msg);
        this.msg=''
    } else {
        var url = "chats/" + this.id2 +"-"+ this.id1 + "/";
        
        this.firebaseService.pushItem(url, msg);
        this.msg=''
      }
  }
   locationselect(){
    this.locationmsg={'lattitude':this.location.coords.latitude,'longitude':this.location.coords.longitude}
   var msg= {"From_id": this.id1,
        "To_id": this.id2,
        "message": this.locationmsg,
        "timestamp": firebase.database.ServerValue.TIMESTAMP,
        "type":'location'}
        console.log(msg);
  
  

        if (this.id1 < this.id2) {

        var url = "chats/" + this.id1 + this.id2 + "/";

        this.firebaseService.pushItem(url, msg);
        this.msg=''
    } else {
        var url = "chats/" + this.id2 + this.id1 + "/";
        
        this.firebaseService.pushItem(url, msg);
        this.msg=''
      }
  }
  dismiss() {
   this.viewCtrl.dismiss();
 }
openLink(lattitude,longitude){
    var link='https://google.com/maps/?q=' +lattitude +',' +longitude;
    const ref  = this.iab.create(link,'_self',{location:'yes'}); 

}
 doInfinite(refresher) {
     
     console.log("scrolled");
     this.scrolledmsg = this.final;
     console.log(this.scrolledmsg.length);
      console.log(this.length);
      
        if (this.length >= this.scrolledmsg.length) {
                this.final = [];
                var length = this.messages.length;
                 var lastkey = this.lastmessagekey;
                  if (this.id1 < this.id2) {
                         var url = "chats/" + this.id1 + this.id2 + "/";
              var query = {
                  query: {

                      orderByKey: 'timestamp',
                      endAt: lastkey,
                      limitToLast: 10
                  }
              };
                this.data = this.firebaseService.listItemQuery(url, query);

              this.data.subscribe((user) => {
                  this.lastmessagekey = user[0].$key;
                  this.final = user;
                  console.log (this.lastmessagekey);
                  this.scrolledmsg.forEach(item => {
                      this.final.push(item);
                      console.log(this.final.length);
                      console.log('srol',this.final);
                  });

              })


                  }else {
              var url = "chats/" + this.id2 + this.id1 + "/";

              console.log("case:false");

              var query = {
                  query: {
                      orderByKey: 'timestamp',
                      endAt: lastkey,
                      limitToLast: 10
                  }
              };
              this.data = this.firebaseService.listItemQuery(url, query);


              this.data.subscribe((user) => {

                  this.lastmessagekey = user[0].$key;
                  this.final = user;
                //   console.log(this.message);
                  this.scrolledmsg.forEach(item => {
                      this.final.push(item);

                      console.log(this.final);
                  });

              })

          }
          setTimeout(() => {
            refresher.complete();
             }, 2000);

        }else{ 
                 console.log('Async operation has ended');
               this.msg_show =false;
                      refresher.complete();
   
    //  
    
        }
 
    //
  }

 

}
