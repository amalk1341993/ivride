import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PancarduploadPage } from './pancardupload';

@NgModule({
  declarations: [
    PancarduploadPage,
  ],
  imports: [
    IonicPageModule.forChild(PancarduploadPage),
  ],
  exports: [
    PancarduploadPage
  ]
})
export class PancarduploadPageModule {}
