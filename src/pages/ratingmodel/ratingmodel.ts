import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';

/**
 * Generated class for the RattingmodalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-ratingmodal',
  templateUrl: 'ratingmodel.html',
})
export class RatingmodelPage {
  // @ViewChild(Nav) nav: Nav;
rate_ride:any;
data:any;
userdata:any;
owner:any;
book_id:any;
rootdb:any;
first_name:any;
  constructor(   public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
		    ) {

     this.rootdb  = firebase.database().ref();
     var users = this.firebaseService.getCurrentUser();
    this.userdata.id=users.uid;

 
     this.data=navParams.get("data");
      this.owner=navParams.get("owner"); 
      this.book_id=navParams.get("book_id");
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatingmodalPage');
  }
  
  rating() {
    console.log(this.rate_ride);
     var ratedata ={'ride_id': this.data,
                    'user_id':this.userdata.uid,
                    'owner':this.owner,
                    'rating':this.rate_ride,
                    'book_id':this.book_id

     }
     console.log(ratedata);
     var res = this.firebaseService.pushItem("rating",ratedata); 
     console.log(res); 
     var dat={car_id:res}
     this.rootdb.child('rating').child(res).update(dat);
     this.rootdb.child('notification').orderByChild('user_id').equalTo(this.owner).once('value',function(snap){
     this.notification = snap.val();
       if(this.notification.email_new_rating==1){
              var request= this.userdata.first_name;
              var subject="Rated";
              var user_id = this.owner;
              var page='confirm_mails/confirm_mail_rate';
              //send email
       }
       if(this.notification.push_ratings==1){
              var request= this.userdata.first_name;
              var subject="Rated";
              var user_id = this.owner;
              var page='confirm_mails/confirm_mail_rate';
              //send email
       }

    })







     this.viewCtrl.dismiss({'rate':this.rate_ride});
      //this.myservice.show_loader();
  }
  pop_close(){
    
     this.viewCtrl.dismiss();
  }

}
