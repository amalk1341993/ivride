import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-request',
  templateUrl: 'request.html'
})
export class Request {
  user_data:any;
  data:any;
  source_sp:any;
  source_head:any;
  source_new:any;
  destination_sp:any;
  destination_head:any;
  destination_new:any;
  rate:any;


  constructor(  public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
			) {
          this.storage.get('userdata').then((userdata) => {
         this.user_data=userdata;
   
		  console.log(this.user_data);
  })
  this.data= navParams.get("data");
  this.rate =navParams.get("rate");
    this.source_sp= this.data.source.split(',');
            this.source_head=this.source_sp[0];
            console.log(this.source_sp);           
              let string_val=this.data.source;
              let toArray = string_val.split(",");
              toArray.splice (0, 1);
              let str=toArray.toString();
              this.source_new=str;
            this.destination_sp= this.data.destination.split(',',2);
            this.destination_head=this.destination_sp[0];
             let string_val1=this.data.destination;
              let toArray1 = string_val1.split(",");
              toArray1.splice (0, 1);
              let str1=toArray.toString();
             

             this.destination_new= str1;
             
   console.log(this.data)
 

  }

  ionViewDidLoad() {
  }

  goBack(){
     this.navCtrl.pop();
   }



   summary() {
   //let modal = this.modalCtrl.create('Summary',{'data':this.data});
   //modal.present();
   this.navCtrl.push('Summary',{'data':this.data});
   }
}
