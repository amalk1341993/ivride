import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Searchmain } from './searchmain';

@NgModule({
  declarations: [
    Searchmain,
  ],
  imports: [
    IonicPageModule.forChild(Searchmain),
  ],
  exports: [
    Searchmain
  ]
})
export class SearchmainModule {}
