import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-searchmain',
  templateUrl: 'searchmain.html'
})
export class Searchmain {
ride_id:any;
user_id:any;
find_data:any;
date:any;
year:any;
date_of_birth:any;
values:any;
age:any;
kmdetails:any;
verified:any;
phne_verified:any;
source_sp:any;
source_head:any[]  =  [];
source_new:any;
destination_sp:any;
destination_head:any;
destination_new:any;
user_data:any;
rate:any;
newdata:any;
comments:any;
avg_rate:any;
avgrate:any;
count:any;
join_date:any;
ride_time:any;
ampm:any;
formatted_time:any;
reached_time:any;
formatted_reached_time:any;
rootdb:any;

  constructor(   public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
             ) {
       this.ride_id = navParams.get("ride_id");
       this.user_id = navParams.get("user_id");
       console.log(this.ride_id);
       console.log(this.user_id);
       this.verified = 1;
       

       this.rootdb  = firebase.database().ref();
        var users = this.firebaseService.getCurrentUser();
         this.user_data=users;

         this.rate = {ratenum:0};

    

      
  }
  show_details(){
    var This = this;
     This.rootdb  = firebase.database().ref();
     //var data={'user_id':this.user_id,'ride_id':this.ride_id}
     This.rootdb.child('ride').child(this.ride_id).once('value',function(snap){
       This.find_data = snap.val();
       This.find_data.reached_time = This.time_cal(This.find_data.departure_time,This.find_data.approximatetime);
       This.find_data.departure_time = This.set_hours(This.find_data.departure_time);
       This.find_data.reached_time = This.set_hours(This.find_data.reached_time);

       This.rootdb.child('users').child(This.find_data.users_id).once('value',function(snap){
         //This.find_data.users = snap.val();
         var users = snap.val();
          This.date=new Date();
          This.year= This.date.getFullYear();
          if(users.dob){
            var dob = users.dob.split("-");
            users.age = This.year-dob[2]+" Y/O";
          } else {
            users.age = '';
          }
          This.find_data.users = users;console.log(This.find_data.destination);
       })

       This.rootdb.child('preference').child(This.find_data.users_id).once('value',function(snap){
         //This.find_data.users = snap.val();
         var preference = snap.val();
          This.find_data.preference = preference;
       })

       This.rootdb.child('car_details').child(This.find_data.car_id).once('value',function(snap){
         //This.find_data.users = snap.val();
         var car_details = snap.val();
          This.find_data.car_details = car_details;
       })

       This.rootdb.child('ride').orderByChild('users_id').equalTo(This.find_data.users_id).once('value',function(snap){
         //This.find_data.users = snap.val();
         var rides = This.myservice.obj_array(snap.val());
         //This.result = This.myservice.obj_array(preference);
          This.count = rides.length;
       })

       


       console.log(This.find_data);
     })
     
      //this.myservice.show_loader();
	    /*this.myservice.load_post(data,"find_ride_detailed").subscribe(response => {
	  	//this.myservice.hide_loader();
   
      console.log(response);
      console.log(response.time);
          if(response.status == "success") {
  
            console.log(response.data);
            this.find_data=response.data;
            this.join_date=this.find_data.join_date;
            this.count=response.user_count.total_count;
            this.source_sp= this.find_data.source.split(',');
            this.source_head=this.source_sp[0];
            console.log(this.source_sp);           
              let string_val=this.find_data.source;
              let toArray = string_val.split(",");
              toArray.splice (0, 1);
              let str=toArray.toString();
              this.source_new=str;
            this.destination_sp= this.find_data.destination.split(',',2);
            this.destination_head=this.destination_sp[0];
             let string_val1=this.find_data.destination;
              let toArray1 = string_val1.split(",");
              toArray1.splice (0, 1);
              let str1=toArray.toString();
             

             this.destination_new= str1;

            console.log(this.source_sp[0]);
            this.find_data.source_head= this.source_head;
            this.find_data.source_new= this.source_new;
             this.find_data.destination_head=  this.destination_head;
            this.find_data.destination_new=  this.destination_new;

            this.verified= this.find_data.email_is_verified;
            this.phne_verified= this.find_data.phone_is_verified;
            //  this.verified= 0;
            console.log(this.verified);
            this.kmdetails=response.kmdetails;
            console.log(this.find_data.source);
              this.date=new Date();
              this.year= this.date.getFullYear();
              if(this.find_data.date_of_birth){
                this.date_of_birth=this.find_data.date_of_birth;
              this.values= this.date_of_birth.split("-");
              this.age=this.year - this.values[0];
              this.find_data.age= this.age;

              }
              this.ride_time= this.find_data.detour_time.split(':');
              this.ampm='AM';
                   if ( this.ride_time[0] >= 12) {
                     this.ampm= 'PM';
                 }
                 if ( this.ride_time[0] > 12) {
                   this.ride_time[0] =  this.ride_time[0] - 12;
               }
               this.formatted_time =  this.ride_time[0] + ':' +  this.ride_time[1] + ':' +  this.ride_time[2] + ' ' +this.ampm;
              
               this.reached_time= this.find_data.reached_time.split(':');
               console.log('reached', this.find_data.reached_time)
               this.ampm='AM';
               if (this.reached_time[0] >= 12) {
                 this.ampm= 'PM';
             }
             if (this.reached_time[0] > 12) {
               this.reached_time[0] =   this.reached_time[0] - 12;
           }
           this.formatted_reached_time =  this.reached_time[0] + ':' +  this.reached_time[1] + ':' +   this.reached_time[2] + ' ' +this.ampm;
           this.find_data.detour_time= this.formatted_time;
           this.find_data.reached_time= this.formatted_reached_time;
              
              console.log(this.find_data);
        
          }
          else {
            this.myservice.show_alert("Error!", response.message);
          }
      })*/

         /*var This = this;
         This.rootdb  = firebase.database().ref(); 
         This.rootdb.child('rating').orderByChild('owner').equalTo(this.user_id).once('value',function(snap){
              this.rate = snap.val();


          })*/




       /*this.myservice.load_post( this.user_id ,"getrate_find_ride").subscribe(response => {
          if(response.status == "success") {
            this.rate =response.data;
            console.log(this.rate.avgrate);
          }

       })*/
  }

  ionViewDidLoad() {
    this.show_details();
  }

  goBack(){
     this.navCtrl.pop();
   }

   time_cal(req_time,res_time){
     var req_array  = req_time.split(':');
     var res_array  = res_time.split(':');
     var hour = parseInt(req_array[0])+parseInt(res_array[0]);
     var min = parseInt(req_array[1])+parseInt(res_array[1]);
     if(min>=60){
       hour = hour+1;
       min = min-60;
     }

     if(hour>=24){
       hour = hour-24;
     }
     var time_min = min>10?min:"0"+min;
     return hour+":"+time_min+":00";
   }

   set_hours(time_val){
     var time_array  = time_val.split(':');
     var hour_val = 'Am';
     if(time_array[0]>12){
       hour_val = 'Pm';
       time_array[0] = time_array[0] - 12;
       time_array[0] = time_array[0]>=10?time_array[0]:"0"+time_array[0];
     }
     return time_array[0]+":"+time_array[1]+" "+hour_val;
   }

   get_join(join_date){
     var d = new Date(join_date);
     var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
     return formattedDate;
   }

   bookpopup(seats) {
     console.log(this.user_data);
     if(this.user_data){
      let modal = this.modalCtrl.create('Bookpopup',{'data':this.find_data,'rate': this.rate,'ride_id':this.ride_id,'user_id':this.user_id,'seats':seats});
      modal.present();
       modal.onDidDismiss(datas => {
         if(datas){
         console.log(datas);
          this.newdata={'ride_id':this.ride_id,'seat':datas.seat};
           this.navCtrl.push('Secureseat',{"ride_id":this.ride_id,"seat":datas.seat,"data":this.find_data});
       }
          })
          

     }else{
         let modal = this.modalCtrl.create('Loginpop');
      modal.present();
       modal.onDidDismiss(data => {
    this.rootdb  = firebase.database().ref();
      var users = this.firebaseService.getCurrentUser();
      
         this.user_data=users;
   
		  
 })
     }

   
   }

   ask() {
   let modal = this.modalCtrl.create('Ask',{'data':this.find_data});
   modal.present();
   }

   contact() {
    if(this.user_data){
      let modal = this.modalCtrl.create('Contact',{'data':this.find_data,'rate': this.rate});
      modal.present();
        }else{
         let modal = this.modalCtrl.create('Loginpop');
      modal.present();
       modal.onDidDismiss(data => {
   this.rootdb  = firebase.database().ref();
      var users = this.firebaseService.getCurrentUser();
    
         this.user_data=users;
  

 })
     }

   }
   reportride(){
       if(this.user_data){
      let modal = this.modalCtrl.create('ReportridePage',{'data':this.find_data});
      modal.present();
       modal.onDidDismiss(data => {
        this.comments=data;
        var reportdata ={'user_id':this.user_data.uid,
                          'owner':this.find_data.id,
                          'comment': this.comments,
                          

        }
        console.log(reportdata);
         this.rootdb  = firebase.database().ref();
        var res = this.firebaseService.pushItem("report_ride",reportdata); 
        console.log(res); 
        var dat={id:res}
        this.rootdb.child('report_ride').child(res).update(dat);
     //        this.rate =response.data;
    console.log(data);
  });
        }else{
         let modal = this.modalCtrl.create('Loginpop');
      modal.present();
       modal.onDidDismiss(data => {
      this.rootdb  = firebase.database().ref();
      var users = this.firebaseService.getCurrentUser();
      

 })
     }

   }

   get_loc(loc){
    var y = loc.split(",");
    return y[0];
  }

  get_left_loc(loc){
    let toArray = loc.split(",");
    toArray.splice (0, 1);
    return toArray.toString();
  }

}
