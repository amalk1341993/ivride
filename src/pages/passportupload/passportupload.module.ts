import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassportuploadPage } from './passportupload';

@NgModule({
  declarations: [
    PassportuploadPage,
  ],
  imports: [
    IonicPageModule.forChild(PassportuploadPage),
  ],
  exports: [
    PassportuploadPage
  ]
})
export class PassportuploadPageModule {}
