import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Panedit } from './panedit';

@NgModule({
  declarations: [
    Panedit,
  ],
  imports: [
    IonicPageModule.forChild(Panedit),
  ],
  exports: [
    Panedit
  ]
})
export class PaneditModule {}
