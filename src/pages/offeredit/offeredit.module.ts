import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OffereditPage } from './offeredit';

@NgModule({
  declarations: [
    OffereditPage,
  ],
  imports: [
    IonicPageModule.forChild(OffereditPage),
  ],
  exports: [
    OffereditPage
  ]
})
export class OffereditPageModule {}
