import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams,ViewController } from 'ionic-angular';
declare var google;


/**
 * Generated class for the OffereditPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-offeredit',
  templateUrl: 'offeredit.html',
})
export class OffereditPage {
data:any;
  place_from:any;
  place_to:any;
  lat_from:any;
  long_from:any;
  lat:any;
  long:any;
  lat_to:any;
  long_to:any;
  mytime:any;
  date:any;
  today:any;
  price:any;
  luggage:any;
  flexibility:any;
  detour:any;
  seats:any;
  car_id:any;
  cardetails_data:any;
  comment:any;
  month:any;
  tom:any;
  rootdb:any;
  usercar:any;
 val:any;
 res:any;
    constructor(public popoverCtrl: PopoverController,
                public modalCtrl: ModalController,
                public navCtrl: NavController,
                private toastCtrl: ToastController,
                private myservice: Myservice,
                public storage: Storage, 
                private formBuilder: FormBuilder,
                public firebaseService:FirebaseService,
                public afAuth: AngularFireAuth,
                public navParams: NavParams,
                 public viewCtrl: ViewController
              ) {
   
    this.data=navParams.get("data");
    console.log(this.data);
    this.place_from =this.data.source;
    this.place_to =this.data.destination;
    this.month=this.data.departure_date;
    console.log(this.data.detour_time);
    var new_tm = this.convertTime12to24(this.data.detour_time);
    console.log(new_tm);
    this.mytime = new_tm;
    
    console.log(this.mytime);
    this.price=this.data.price;
    this.luggage=this.data.max_luggage;
    
    console.log(this.luggage);
    this.flexibility=this.data.pickup_flexibility;
    this.detour=this.data.detour;
    this.seats=this.data.no_of_seats;
    this.today=new Date();
   
    this.comment=this.data.comments;
    this.lat_from=this.data.source_lat;
    this.long_from=this.data.source_lng;
    this.lat_to=this.data.destination_lat;
    this.long_to=this.data.destination_lng; 
    this.date = this.today.getFullYear()+'-'+(this.today.getMonth()+1)+'-'+this.today.getDate();
     console.log(this.date);
     var myDate = new Date();
     var nextDay = new Date(myDate);
     nextDay.setDate(myDate.getDate()+1);
     this.tom = nextDay.toISOString();

     this.car_details();

     var This=this;
           This.rootdb  = firebase.database().ref(); 
           This.rootdb.child('car_details').on('value',function(snap){
            This.car_id = snap.val(); //console.log(This.charge.kmcharge);
          }) 

  } 

  convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(' ');
    console.log(time,"=", modifier);
  
    let [hours, minutes] = time.split(':');
    console.log(hours, minutes);
  
    if (hours === '12') {
      hours = '00';
    }
  
    if (modifier == 'PM'|| modifier == 'pm') {
      hours = parseInt(hours, 10) + 12;
    }
  
    return hours + ':' + minutes+':'+ '00';
  }

  car_details(){
    var This=this;
        this.rootdb.child('car_details').on('value',function(snap){
           This.usercar = snap.val(); //console.log(This.usercar);
              if(This.usercar!=null && This.usercar!='' ){
                   var new_array = new Array();
                  for(var i in This.usercar){
                    if(This.usercar[i].user_id==this.data.users_id){
                    new_array.push(This.usercar[i]);
                   }
                  }
                  This.cardetails_data = new_array;
            }
          })
}
   ionViewDidLoad() {
    console.log('ionViewDidLoad OffereditPage');
  }
    autocompleteFocusfrom(){
         var input = document.getElementById('placefrom');
    console.log("hauuuu");
    console.log(input);
    var options = {
      // componentRestrictions: {
      //   country: 'in'
      // }
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
    var This=this;
    google.maps.event.addListener(autocomplete, 'place_changed', function () {

      var place = autocomplete.getPlace();
    
      This.place_from = place.formatted_address;
        console.log(this.place_from);
      This.lat_from = place.geometry.location.lat(); console.log(This.lat_from);
      This.long_from = place.geometry.location.lng();console.log(This.long_from);
      
    });
    this.place_from = This.place_from;
    this.lat_from=  This.lat_from;
  console.log( this.lat_from);
    this.long_from =  This.long_from ;
      console.log(  this.long_from);
   }
        autocompleteFocusto(){
         var input = document.getElementById('placeto');
    console.log("hauuuu");
    console.log(input);
    var options = {
      // componentRestrictions: {
      //   country: 'in'
      // }
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
var This=this;
    google.maps.event.addListener(autocomplete, 'place_changed', function () {

      var place = autocomplete.getPlace();
      This.place_to= place.formatted_address;
      //  console.log(This.place_to);
      This.lat_to = place.geometry.location.lat();
      This.long_to = place.geometry.location.lng();
    });
    this.place_to= This.place_to;
    this.lat_to= This.lat_to;
    console.log(this.lat_to);
    this.long_to = This.long_to;
    }
    
  // public event = {
  //     month: '2016-11-01',
  //     timeStarts: '07:43',
  //     timeEnds: '2050-02-20',
  //   }
    
decrement(){
if(this.price>10){
  this.price=Number(this.price)-50;
   console.log(this.price);
}
 

}
increment(){
  this.price=Number(this.price)+50;
     console.log(this.price);

}
incrementseats(){
   if(this.seats<5){
  this.seats= Number(this.seats)+1;
  console.log(this.seats);
  }
  

}
decrementseats(){
  if(this.seats>=1){
  this.seats=Number(this.seats)-1;
   }
 
  
}
  addcomment() {
  let modal = this.modalCtrl.create('Addcomment',{'datanew':this.comment});
  modal.present();
  modal.onDidDismiss(data => {
    this.comment=data;
    console.log(data);
  });
}
edit(){ 
  console.log(this.mytime)
  var departure_date = this.month.split("T");
  this.month = departure_date[0];
  if(this.car_id== undefined){
    this.myservice.show_alert("Error!",'please select car');
 }
 else if(this.seats==0){
   this.myservice.show_alert("Error",'select Atleast one seat');

 }else if(this.price==0){
   this.myservice.show_alert("Error",'Price should be greater than 0');
 }else{
  var update_data={ 'destination': this.place_to,
  'source': this.place_from, 
  'departure_date': this.month,
  'detour_time':this.mytime,  
  'source_lat':this.lat_from,
  'source_lng':this.long_from,
  'destination_lat':this.lat_to,
  'destination_lng':this.long_to, 
  'price':this.price,
 'no_of_seats':this.seats,
 'max_luggage':this.luggage,
 'detour':this.detour,
 'pickup_flexibility':this.flexibility,
 'comments':this.comment,
 'users_id':this.data.users_id,
 'car_id':this.car_id,
 'id':this.data.id

}
console.log(update_data);
//this.myservice.show_loader();
//this.myservice.load_post(update_data,"offer_edit").subscribe(response => {
  var This=this;
  This.rootdb.child('ride').orderByChild('car_id').equalTo(this.car_id).once('value',function(snap){

     This.res = snap.val();
     if(This.res){
        var new_array = new Array();
        for(var i in This.res){
          if(This.res[i].departure_time==this.month){
                    new_array.push(This.res[i]);

                  }
                 This.val = new_array;console.log(This.res);
        }
        for(var i in This.val){
         if(This.val[i].detour_time<=this.mytime && This.val[i].reached_time>=this.mytime && This.val[i].id!=this.data.id){
             this.rootdb.child('ride').child(this.data.id).update(update_data);
         }
       }

     }
 })

   This.rootdb.child('alert').orderByChild('sour_dest_date').equalTo(this.place_from+'#'+this.place_to+'#'+this.month).once('value',function(snap){
   //This.rootdb.child('alert').orderByChild('source').equalTo(this.place_from).orderByChild('departure_date').equalTo(this.month).
               //orderByChild('destination').equalTo(this.place_to).once('value',function(snap){
                 This.res = snap.val();
                 var new_array = new Array();
                 for(var i in This.res){
                  if(This.res[i].departure_time<=this.mytime && This.res[i].before_time>=this.mytime){
                    new_array.push(This.res[i]);

                  }
                  This.res = new_array;console.log(This.res);
              }

            for(var i in This.res.user_id){
              
                //send alert

            }

    })

  this.viewCtrl.dismiss();
  //this.navCtrl.setRoot('Yourride',this.navCtrl.getActive().component);
    //this.myservice.hide_loader();
  this.navCtrl.push('Yourride');
 // this.navCtrl.push('Yourride');
 
 



//})




}
   
 }

 goBack(){
  this.navCtrl.push('Yourride');
 }

}
