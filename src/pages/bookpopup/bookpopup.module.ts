import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Bookpopup } from './bookpopup';

@NgModule({
  declarations: [
    Bookpopup,
  ],
  imports: [
    IonicPageModule.forChild(Bookpopup),
  ],
  exports: [
    Bookpopup
  ]
})
export class BookpopupModule {}
