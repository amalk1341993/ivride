import { Component } from '@angular/core';

import {IonicPage,NavController,ViewController,ModalController,NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-bookpopup',
  templateUrl: 'bookpopup.html'
})
export class Bookpopup {
  seats:any;
  avail_seat:any;
  accept=true;
  constructor(public navCtrl: NavController,public viewCtrl: ViewController,public modalCtrl: ModalController,public navParams: NavParams,) {
    this.seats = navParams.get("seats");
    this.avail_seat = navParams.get("seats");
    this.accept = true;
  }

  ionViewDidLoad() {
  }

  secureseat() {
  //let modal = this.modalCtrl.create('Secureseat');
  //modal.present();
  var data = {'seat':this.seats};
  this.viewCtrl.dismiss(data);
  }

  decrement(){
    if(this.seats>1){
      this.seats = this.seats-1;
    } else {
      this.seats = 1;
    }
  }
  increment(){
    if(this.seats<this.avail_seat){
      this.seats = this.seats+1;
    } else {
      this.seats = this.avail_seat;
    }
  }
  change_fun(){
    console.log(this.accept)
    this.accept = this.accept==true?false:true;
  }

  get_value(){
    return this.accept;
  }

}
