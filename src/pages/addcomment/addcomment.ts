import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,NavParams } from 'ionic-angular';
IonicPage
@IonicPage()
@Component({
  selector: 'page-addcomment',
  templateUrl: 'addcomment.html'
})
export class Addcomment {
  // modalvalue:any;
  data:any;
 

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
      //  this.modalvalue=navParams.get("data");
       this.data=navParams.get("datanew");
       
       console.log( this.data);
  }

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss( this.data);
 }

}
