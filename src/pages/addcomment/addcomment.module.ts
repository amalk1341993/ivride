import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addcomment } from './addcomment';

@NgModule({
  declarations: [
    Addcomment,
  ],
  imports: [
    IonicPageModule.forChild(Addcomment),
  ],
  exports: [
    Addcomment
  ]
})
export class AddcommentModule {}
