import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Offered } from './offered';

@NgModule({
  declarations: [
    Offered,
  ],
  imports: [
    IonicPageModule.forChild(Offered),
  ],
  exports: [
    Offered
  ]
})
export class OfferedModule {}
