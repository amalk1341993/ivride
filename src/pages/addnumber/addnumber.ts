import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-addnumber',
  templateUrl: 'addnumber.html'
})
export class Addnumber {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {}

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }

}
