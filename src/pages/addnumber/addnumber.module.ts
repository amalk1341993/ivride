import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addnumber } from './addnumber';

@NgModule({
  declarations: [
    Addnumber,
  ],
  imports: [
    IonicPageModule.forChild(Addnumber),
  ],
  exports: [
    Addnumber
  ]
})
export class AddnumberModule {}
