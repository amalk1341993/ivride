import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams,ViewController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-publicprofile',
  templateUrl: 'publicprofile.html'
})
export class Publicprofile {
 userdata:any;
  year:any;
  is_photo:any;
  is_phone:any;
  rootdb:any;
 data:any;
 currentUser: any;
 uid:any;
 email:any;
 usercar:any;
   profile_pic: any;
   profile_photo:any;
   email_is_verified:any;
   id:any;
   day:any;
   month:any;
  constructor(
  			  public popoverCtrl: PopoverController,
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              public storage: Storage, 
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public navParams: NavParams,
              public viewCtrl: ViewController,
             ) {
        var This=this;
        this.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
           if (users.emailVerified === true) {
                 //console.log("verified");
                 this.email_is_verified=1;
               }
          This.uid = users.uid;
          this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.userdata = snap.val();
            if(This.userdata.profile_photo!=null && This.userdata.profile_photo!='' ){
              this.is_photo = This.userdata.profile_photo;
            }
          })
                 if(This.userdata.dob!=null && This.userdata.dob!='' ){
                 	 var currentYear = new Date().getFullYear();
			          var dob = This.userdata.dob.split("-");
			          //this.year = dob[0];
			         // this.year = currentYear - dob[0];
			          this.day = dob[0];
         			  this.month = dob[1];
          			  this.year = dob[2];
			        }

        }
        });






  }

  ionViewDidLoad() {
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }

}
