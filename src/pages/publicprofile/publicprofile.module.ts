import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Publicprofile } from './publicprofile';

@NgModule({
  declarations: [
    Publicprofile,
  ],
  imports: [
    IonicPageModule.forChild(Publicprofile),
  ],
  exports: [
    Publicprofile
  ]
})
export class PublicprofileModule {}
