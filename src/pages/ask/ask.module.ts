import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ask } from './ask';

@NgModule({
  declarations: [
    Ask,
  ],
  imports: [
    IonicPageModule.forChild(Ask),
  ],
  exports: [
    Ask
  ]
})
export class AskModule {}
