import { Component,ViewChild } from '@angular/core';
import { IonicPage,NavController,Slides} from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class Landing {
  @ViewChild('mySlider') slider: Slides;
  showPrev:any;
showNext:any;
currentIndex:any;
  constructor(public navCtrl: NavController) {
this.currentIndex = 0;

  }

  goToNextSlide() {
  	  this.slider.slideNext();
	  this.slider.getActiveIndex();
    }
    goToPrevSlide() {
  	  this.slider.slidePrev();
	  this.slider.getActiveIndex();
    }
    slideChanged() {
      this.currentIndex = this.slider.getActiveIndex();
    }

    home() {
    this.navCtrl.push('Home');
  }

  login() {
  this.navCtrl.push('Login');
}

signup() {
this.navCtrl.push('Signup');
}
}
