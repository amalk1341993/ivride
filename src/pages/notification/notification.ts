import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams,ViewController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html'
})
export class Notification {
	rootdb:any;
 notify = {userid :'',push_cotraveller_confirmation:'',push_msg_recieved:'',push_ratings:'',email_ride_publish:'',
  email_ride_update:'',email_msg_recieved:'',email_pending_ratings:'',email_new_rating:'',email_info:'',push_cotraveller_message:''};
  constructor( public popoverCtrl: PopoverController,
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public navParams: NavParams,
              public viewCtrl: ViewController
              ) {}

  ionViewDidLoad() {
  	var users = this.firebaseService.getCurrentUser();
	 this.notify.userid = users.uid;
	 var This = this;
	 	 this.rootdb  = firebase.database().ref();
          this.rootdb.child('notification').orderByChild('user_id').equalTo(this.notify.userid).once('value',function(snap){
           var notify = snap.val(); console.log(notify);
           for(var i in notify){
           	This.notify = notify[i];
           	console.log(This.notify);
           }
          })
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }
 save_notification(){
 	var users = this.firebaseService.getCurrentUser();
 	console.log(users.uid);
     var data = {  
 				"push_cotraveller_confirmation":this.notify.push_cotraveller_confirmation,
 				"push_msg_recieved":this.notify.push_msg_recieved,
 				"push_ratings":this.notify.push_ratings,
				"push_cotraveller_message":this.notify.push_cotraveller_message,
				"email_ride_publish":this.notify.email_ride_publish,
				"email_ride_update":this.notify.email_ride_update,
				"email_msg_recieved":this.notify.email_msg_recieved,
				"email_pending_ratings":this.notify.email_pending_ratings,
				"email_new_rating":this.notify.email_new_rating,
				"email_info":this.notify.email_info,
                "user_id":users.uid
		};
		console.log(data);
      this.rootdb.child('notification/').child(users.uid).set(data);
      let toast = this.toastCtrl.create({
	      message: 'Successfully Added',
	      duration: 2000,
	      position: 'bottom'
      });
      toast.present();
      this.viewCtrl.dismiss(); 
 }
  set_push_msg(event){
    this.notify.push_msg_recieved = event.target.checked;
  }

  set_co_travaller(event){
    this.notify.push_cotraveller_confirmation = event.target.checked;
  }

  set_ratings(event){
    this.notify.push_ratings = event.target.checked;
  }

   set_ride_publish(event){
    this.notify.email_ride_publish = event.target.checked;
  }

    set_ride_update(event){
    this.notify.email_ride_update = event.target.checked;
  }

   set_email_msg(event){
    this.notify.email_msg_recieved = event.target.checked;
  }

   set_pend_rating(event){
    this.notify.email_pending_ratings = event.target.checked;
  }

    set_new_rating(event){
    this.notify.email_new_rating = event.target.checked;
  }

    set_info(event){
    this.notify.email_info = event.target.checked;
  }
  set_cotraveller_message(event){
    this.notify.push_cotraveller_message = event.target.checked;
  }
get_checked(field){
  	console.log(field);
    return field==0?false:true;
  }
  check_true(event) {
  	if(event==true){
            return true;
        } else {
            return false;
        }
  }
}
