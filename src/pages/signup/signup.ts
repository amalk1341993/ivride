import { Component } from '@angular/core';
import { IonicPage,NavController,ModalController,ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class Signup {
private signup : FormGroup;

     mobile:any;
     email:any;
     password:any;
     user:any;
      first_name:any;
     last_name:any;
     biography:any;
     rootdb:any;
    constructor(public navCtrl: NavController,public modalCtrl: ModalController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              public storage: Storage, 
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth
              )
  {
	   this.signup = this.formBuilder.group({
              	 	 mobile:['', Validators.compose([
                        				 Validators.minLength(8),
                        				 Validators.maxLength(15),
                        				 Validators.pattern('[0-9]+$'),
                        				 Validators.required 
                               ])],
              	 	 email: ['', Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])],
              	 	 password: ['', Validators.required],
					         first_name: ['', Validators.required],
					         last_name: ['', Validators.required]
	             });
  }

  ionViewDidLoad() {
  }
signupForm() {
var This = this;
this.rootdb  = firebase.database().ref();
this.rootdb.child('users').orderByChild('email').equalTo(this.signup.value.email).once('value',function(snap){
   
   if(snap.exists()!=true){
    This.afAuth.auth.createUserWithEmailAndPassword(
      This.signup.value.email,
      This.signup.value.password
    ).then((success) => {
      This.afAuth.auth.onAuthStateChanged((user) => { // getting new user
        if (user) {
          if(user.emailVerified === false){
         user.sendEmailVerification().then(function(){
          console.log("email verification sent to mail");
        });
          }
		      var today = new Date();console.log(today);
          //var This = this;
              var userObj ={   
                user_id: user.uid,                          //Create a user object
				        first_name:This.signup.value.first_name,
				        last_name:This.signup.value.last_name,
                email: This.signup.value.email,
                mobile: This.signup.value.mobile,
				join_date:today,
                createdAt: firebase.database.ServerValue.TIMESTAMP
              };
              This.firebaseService.registerUser(user.uid,userObj).then(()=>{ // update user to db
              This.navCtrl.push('Login'); 
              })
           }
      });
    });  
}
else{
      let toast = This.toastCtrl.create({
        message: 'Email Already Exist',
        duration: 2000,
        position: 'bottom'
      });
        toast.present();


}
	  })
}

goBack(){
  const index = this.navCtrl.getActive().index;
  console.log(index);
  if(index==0){
      this.navCtrl.push('Home');
  } else {
    this.navCtrl.pop();
  }
 }

}
