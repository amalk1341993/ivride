import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,PopoverController,ModalController,NavParams,ToastController } from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import {FirebaseService} from '../../providers/firebaseservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@IonicPage()
@Component({
  selector: 'page-passportedit',
  templateUrl: 'passportedit.html'
})
export class Passportedit {
   userdata:any;
	passport = {userid :'',pass_no: '',full_name: '',type:'3',day:'',month:'',year:'',image_file:''};
	dd = [];
	mm = [];
  	yy = [];
  	rootdb:any;
   constructor(public navCtrl: NavController,
               private myservice: Myservice, 
               public storage: Storage, 
               public viewCtrl: ViewController,
               private toastCtrl: ToastController, 
               public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public popoverCtrl: PopoverController,
              public navParams: NavParams
              ) {
   	this.passport.image_file = this.navParams.get('upload_image');
   }
//    ionViewDidLoad() {
// 	this.storage.get('userdata').then((userdata) => {
// 		this.userdata=userdata;
// 		console.log('this.userdata');
// 		this.passport.userid = this.userdata.id;

// 		//this.editbio.biography = this.userdata.biography;
// 	})
//   }


  ionViewDidLoad() {
	//for days
	for (var i = 1; i <= 31; i++) {
				this.dd.push(i);
		}
	//for months
	this.mm.push({'value':'01', 'text':'Jan'});
	this.mm.push({'value':'02', 'text':'Feb'});
	this.mm.push({'value':'03', 'text':'Mar'});
	this.mm.push({'value':'04', 'text':'Apr'});
	this.mm.push({'value':'05', 'text':'May'});
	this.mm.push({'value':'06', 'text':'Jun'});
	this.mm.push({'value':'07', 'text':'Jul'});
	this.mm.push({'value':'08', 'text':'Aug'});
	this.mm.push({'value':'09', 'text':'Sep'});
	this.mm.push({'value':'10', 'text':'Oct'});
	this.mm.push({'value':'11', 'text':'Nov'});
	this.mm.push({'value':'12', 'text':'Dec'});
	//for years
	var currentYear = new Date().getFullYear();
	for (var j = currentYear-10; j > currentYear-70; j--) {
				this.yy.push(j);
		}
	         var This = this;
        This.rootdb  = firebase.database().ref(); 
        var users = this.firebaseService.getCurrentUser();
	}



   passform() {

	//this.myservice.show_loader();
	this.passport.userid = this.userdata.id;
	//console.log('this.adhar.user_id');
		//this.myservice.hide_loader();
			 var data = {  
                         "full_name":this.userdata.userid,
                         "number":this.passport.pass_no,
                         "dob":this.passport.year+'-'+this.passport.month+'-'+this.passport.day,                         

         };
 this.rootdb.child('verification').child(this.passport.userid).child("passport").update(data);

			let toast = this.toastCtrl.create({
				message: "Sucessfully updated",
				duration: 2000,
				position: 'bottom'
			});
		    toast.present();
		    this.viewCtrl.dismiss();
			//this.navCtrl.push('Login'); 
			this.navCtrl.push('Editid');
	
	
	  
	// OTP verification  
	/* let modal = this.modalCtrl.create('Verification');
	modal.present(); */
  }




  dismiss() {
   this.viewCtrl.dismiss();
 }

 goBack(){
    this.navCtrl.pop();
  }



}
