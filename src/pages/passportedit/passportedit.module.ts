import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Passportedit } from './passportedit';

@NgModule({
  declarations: [
    Passportedit,
  ],
  imports: [
    IonicPageModule.forChild(Passportedit),
  ],
  exports: [
    Passportedit
  ]
})
export class PassporteditModule {}
