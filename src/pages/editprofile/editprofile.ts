import { Component, ViewChild } from '@angular/core';
import { IonicPage,NavController,ViewController,ModalController,ToastController } from 'ionic-angular';
import { Nav, Platform,Events,MenuController } from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {FirebaseService} from '../../providers/firebaseservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html'
})
export class Editprofile {
  dd = [];
  mm = [];
  yy = [];
  email:any;
  uid:any;
  mobile:any;
  edit_user ={day:'',f_name:'',l_name:'',month:'',year:'',userid:'',gender:'',email:'',mobile:''};
  currentUser: any;
 public update_profile: FormGroup;
  rootdb:any;
  constructor(  public platform: Platform,
                public statusBar: StatusBar, 
                public splashScreen: SplashScreen,
                public firebaseService:FirebaseService,
                public afAuth: AngularFireAuth,  
                public events: Events,
                public menuCtrl: MenuController,
                public modalCtrl:ModalController,
                public viewCtrl:ViewController,
                private formBuilder: FormBuilder,
                private myservice: Myservice,
                private toastCtrl: ToastController
                ) 
              { 
                var This = this;
                this.rootdb  = firebase.database().ref();
                const unsubscribe = firebase.auth().onAuthStateChanged( user => {
                  if (user) {
                    var users = this.firebaseService.getCurrentUser();
                    this.rootdb.child('users').child(users.uid).on('value',function(snap){
                      This.currentUser = snap.val();
                      //console.log(This.currentUser);
                    })
                  }
                });
                this.update_profile =  this.formBuilder.group({
                                        'mobile':['', Validators.compose([
                                                                            Validators.maxLength(16),
                                                                            Validators.minLength(10),
                                                                            Validators.pattern('[0-9]+$'), 
                                                                            Validators.required 
                                                                        ])
                                                  ],
                                        'gender' : [null, Validators.required],
                                        'f_name' : [null, Validators.required],
                                        'l_name' : [null, Validators.required],
                                        'email' : [null, Validators.required],
                                        'day' : [null, Validators.required],
                                        'month' : [null, Validators.required],
                                        'year' : [null, Validators.required],

                });
              }

  ionViewDidLoad() {
      //for days
    for (var i = 1; i <= 31; i++) {
          this.dd.push(i);
      }
    //for months
    this.mm.push({'value':'01', 'text':'Jan'});
    this.mm.push({'value':'02', 'text':'Feb'});
    this.mm.push({'value':'03', 'text':'Mar'});
    this.mm.push({'value':'04', 'text':'Apr'});
    this.mm.push({'value':'05', 'text':'May'});
    this.mm.push({'value':'06', 'text':'Jun'});
    this.mm.push({'value':'07', 'text':'Jul'});
    this.mm.push({'value':'08', 'text':'Aug'});
    this.mm.push({'value':'09', 'text':'Sep'});
    this.mm.push({'value':'10', 'text':'Oct'});
    this.mm.push({'value':'11', 'text':'Nov'});
    this.mm.push({'value':'12', 'text':'Dec'});
    //for years
    var currentYear = new Date().getFullYear();
    for (var j = currentYear-10; j > currentYear-70; j--) {
          this.yy.push(j);
      }
    var This = this;
    this.rootdb  = firebase.database().ref();
    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
    if (user) {
      var users = this.firebaseService.getCurrentUser();
      This.uid = users.uid;
      this.rootdb.child('users').child(users.uid).on('value',function(snap){
        This.currentUser = snap.val();
        This.edit_user.f_name = This.currentUser.first_name;
        This.edit_user.l_name = This.currentUser.last_name;
        This.edit_user.email = This.currentUser.email;
        This.edit_user.mobile = This.currentUser.mobile;
        This.edit_user.userid = This.currentUser.id;
        This.edit_user.gender = This.currentUser.gender;
        if(This.currentUser.dob!=null && This.currentUser.dob!='' ){
          var dob = This.currentUser.dob.split("-");
          This.edit_user.day = dob[0];
          This.edit_user.month = dob[1];
          This.edit_user.year = dob[2];
        }
      })
    }
    });
  }


  editbio() {
  let modal = this.modalCtrl.create('Editbio');
  modal.present();
  }
  edit_profile()
  {
      var This = this;
      var userObj ={  
                first_name:This.update_profile.value.f_name,
                last_name:This.update_profile.value.l_name,
                email: This.update_profile.value.email,
                mobile: This.update_profile.value.mobile,
                gender: This.update_profile.value.gender,
                dob:This.update_profile.value.day+'-'+This.update_profile.value.month+'-'+This.update_profile.value.year,
                createdAt: firebase.database.ServerValue.TIMESTAMP
      };
      console.log(userObj)
      var users = this.firebaseService.getCurrentUser();
      var res = this.rootdb.child('users').child(this.uid).update(userObj); // update user to db
      let toast = this.toastCtrl.create({
          message: 'Successfully Updated',
          duration: 2000,
          position: 'bottom'
        });
          toast.present();
  }

  editphoto() {
  let modal = this.modalCtrl.create('Editphoto');
  modal.present();
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }

}


