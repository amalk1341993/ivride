import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { IonicPage,NavController,PopoverController,ModalController,ToastController,NavParams,AlertController } from 'ionic-angular';
declare var google:any;
@IonicPage()

@Component({
  selector: 'page-offerride',
  templateUrl: 'offerride.html'
})

export class Offerride {
offerride:FormGroup;
time:any;
place_to:any;
place_from:any;
return:any;
round = false;
lat:any;
lat_from:any;
long:any;
long_from:any;
lat_to:any;
long_to:any;
userdata:any;
user_id:any;
today:any;
date:any;
time_crnt:any;
max_date: any;
test_date:any;
tom:any;
month1: String;
month2: String;
tom1:any;
  rootdb:any;
  uid:any;
  currentUser:any;
  constructor( public popoverCtrl: PopoverController,
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              private toastCtrl: ToastController,
              private myservice: Myservice,
              public storage: Storage, 
              private formBuilder: FormBuilder,
              public firebaseService:FirebaseService,
              public afAuth: AngularFireAuth,
              public navParams: NavParams,
              public alertCtrl :AlertController
             
             
              ) {
                 this.offerride = formBuilder.group({
                  'place_from' : ['', Validators.required],
                  'place_to':['', Validators.required],
                  'round_trip':"",
                  'dep_date':['', Validators.required],
                  'rtrn_date':"",
                  'myDate':"",
                  'time_rtrn':"",
                  'time_dep':['', Validators.required]
                  
                });
         var This = this;
        this.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
          This.uid = users.uid;
          this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.currentUser = snap.val();
          })
   
        }
    })
      var myDate = new Date();
      var nextDay = new Date(myDate);
      nextDay.setDate(myDate.getDate()+1);
      this.tom = nextDay.toISOString();
      this.tom1=nextDay.toISOString();
      this.month1=this.tom ;
      this.month2=this.tom1 ;
      this.return = true;

  }

  ionViewDidLoad() {
  }

  public event = {
      month: '2016-11-01',
      timeStarts: '07:43',
      timeEnds: '2050-02-20'
    }
  goBack(){
     this.navCtrl.pop();
   }

 /*  offerridedetail() {
   this.navCtrl.push('Offerridedetail');
 }*/
   autocompleteFocusfrom(){
    var input = document.getElementById('placefrom');
   // console.log(input);
    var options = {
      // componentRestrictions: {
      //   country: 'in'
      // }
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
    var This=this;
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      var place = autocomplete.getPlace();
      This.place_from = place.formatted_address;
      //console.log(This.place_from);
      This.lat_from = place.geometry.location.lat();
      This.long_from = place.geometry.location.lng();
    });
    this.place_from = This.place_from;
    this.lat_from= This.lat;
    console.log(this.lat_from);
    this.long_from = This.long;
   }
   autocompleteFocusto(){
    var input = document.getElementById('placeto');
   // console.log(input);
    var options = {
      // componentRestrictions: {
      //   country: 'in'
      // }
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
    var This=this;
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      var place = autocomplete.getPlace();
      This.place_to= place.formatted_address;
       console.log(This.place_to);
      This.lat_to = place.geometry.location.lat();
      This.long_to = place.geometry.location.lng();
    });
    this.place_to= This.place_to;
    this.lat_to= This.lat_to;
    console.log(this.lat_to);
    this.long_to = This.long_to;
    }
    check_roundtrip(value){
    this.round = !this.round;
    console.log(this.round);
    if(this.round ==true){
       this.return = false;
    }
    else{
       this.return = true;

    }
   
  }
   offerrideForm($event,post){
    //console.log(post.round_trip) ;
               var departure_date = post.dep_date.split("T");
               post.dep_date = departure_date[0];
                var rtrn_date = post.rtrn_date.split("T");
                post.rtrn_date = rtrn_date[0];
     var This=this;
     var data={
              'destination':This.place_to,
              'source':This.place_from, 
              'round_trip':post.round_trip,  
              'departure_date':post.dep_date,
              'detour_time':post.time_dep,
              'rtrn_date':post.rtrn_date,
              'rtrn_time':post.time_rtrn,  
              'source_lat':this.lat_from,
              'source_lng':this.long_from,
              'sour_dest_date':This.place_from+"#"+This.place_to+"#"+post.dep_date,
              'destination_lat':this.lat_to,
              'destination_lng':this.long_to, 
              'user_id':this.user_id,
              'approximatetime':post.approximatetime
     }
     console.log(data);
     var posted_data = data;
    
       if(this.currentUser){
           //this.myservice.show_loader();

          var source = This.place_from;
          var x = source.split(",");
         // console.log(x[0]);
          var destination = This.place_to;
          var y= destination.split(",");
         // console.log(y[0]);
         
                 this.navCtrl.push('Offerridedetail',{
               'firstPassed': x[0],
               'secondPassed': y[0],
              'thirdPassed':data,
               'fourthPassed':post.round_trip,
               'data':data
              
              
                });
       } else {
          let modal = this.modalCtrl.create('Loginpop');
            modal.present();
            modal.onDidDismiss(data => { 
            console.log("logged successfully");
             var users = this.firebaseService.getCurrentUser();
             console.log(users)
             if(users!=null){
               this.uid = users.uid;
               var source = This.place_from;
               var x = source.split(",");
               var destination = This.place_to;
               var y= destination.split(",");
               var params = {'firstPassed': x[0],'secondPassed': y[0],'thirdPassed':posted_data,'fourthPassed':post.round_trip,'data':posted_data};
               console.log(params);
               This.navCtrl.push('Offerridedetail',params);
             }

            })
       }
  
      
   }

}
