import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Offerride } from './offerride';

@NgModule({
  declarations: [
    Offerride,
  ],
  imports: [
    IonicPageModule.forChild(Offerride),
  ],
  exports: [
    Offerride
  ]
})
export class OfferrideModule {}
