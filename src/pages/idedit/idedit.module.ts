import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Idedit } from './idedit';

@NgModule({
  declarations: [
    Idedit,
  ],
  imports: [
    IonicPageModule.forChild(Idedit),
  ],
  exports: [
    Idedit
  ]
})
export class IdeditModule {}
