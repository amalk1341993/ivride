import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ModalController,ToastController } from 'ionic-angular';
import { Nav, Platform,Events,MenuController } from 'ionic-angular';
import {Myservice} from '../../../providers/myservice';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {FirebaseService} from '../../../providers/firebaseservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ImagePicker } from '@ionic-native/image-picker';
@IonicPage()
@Component({
  selector: 'page-editphoto',
  templateUrl: 'editphoto.html'
})
export class Editphoto {
 rootdb:any;
 data:any;
 currentUser: any;
 uid:any;
   profile_pic: any;
  public base64Image : string;
  constructor(  public navCtrl: NavController,
  				public platform: Platform,
                public statusBar: StatusBar, 
                public splashScreen: SplashScreen,
                public firebaseService:FirebaseService,
                public afAuth: AngularFireAuth,  
                public events: Events,
                public menuCtrl: MenuController,
                public modalCtrl:ModalController,
                public viewCtrl:ViewController,
                private formBuilder: FormBuilder,
                private myservice: Myservice,
                private toastCtrl: ToastController,
                private transfer: FileTransfer,
                private camera: Camera,
                private imagePicker: ImagePicker

                ) {}

  ionViewDidLoad() {
  	 var This = this;
    this.rootdb  = firebase.database().ref();
    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
    if (user) {
      var users = this.firebaseService.getCurrentUser();
      This.uid = users.uid;
      this.rootdb.child('users').child(users.uid).on('value',function(snap){
        This.currentUser = snap.val();
        if(This.currentUser.profile_photo!=null && This.currentUser.profile_photo!='' ){
          this.profile_photo = This.currentUser.profile_photo;
        }
      })
    }
    });
  }
  take_picture() {
      var This = this;
  	const options: CameraOptions = {
  	  quality: 75,
  	  destinationType: This.camera.DestinationType.FILE_URI,
  		encodingType: This.camera.EncodingType.JPEG,
      mediaType: This.camera.MediaType.PICTURE,
      sourceType:1
  	}
  		This.camera.getPicture(options).then((imageData) => {
  		// imageData is either a base64 encoded string or a file URI
  		// If it's base64:
  		This.base64Image = imageData;
  		console.log(This.base64Image);
  		    
            var data ={ profile_picture:This.data };
            console.log(data)
            var users = This.firebaseService.getCurrentUser();
           
            var res = This.rootdb.child('users').child(This.uid).update(data); // update user to db
            let toast = This.toastCtrl.create({
                message: 'Successfully Uploaded',
                duration: 2000,
                position: 'bottom'
              });
                toast.present();



  	}, (err) => {
  		console.log(err);
  	});
  } 
    uploadPicture(){ 
       var This = this;
  	This.imagePicker.getPictures({maximumImagesCount:1}).then((results) => {
	  for (var i = 0; i < results.length; i++) {
		  console.log('Image URI: ' + results[i]);
		  This.base64Image = results[i];
		  This.profile_pic = results[i];
		            var data ={ profile_picture:This.profile_pic };
                console.log(data)
                var users = This.firebaseService.getCurrentUser();
              
                var res = This.rootdb.child('users').child(This.uid).update(data); // update user to db
                let toast = This.toastCtrl.create({
                    message: 'Successfully Uploaded',
                    duration: 2000,
                    position: 'bottom'
                  });
                    toast.present();
	  }
	}, (err) => { 
			console.log(err);
		});
  }
  dismiss() {
   this.viewCtrl.dismiss();
 }


}
