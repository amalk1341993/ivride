import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Editphoto } from './editphoto';

@NgModule({
  declarations: [
    Editphoto,
  ],
  imports: [
    IonicPageModule.forChild(Editphoto),
  ],
  exports: [
    Editphoto
  ]
})
export class EditphotoModule {}
