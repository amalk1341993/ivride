import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';
@IonicPage()
@Component({
  selector: 'page-idedit',
  templateUrl: 'idedit.html'
})
export class Idedit {
	rootdb:any;
   userdata:any;
   currentUser: any;
 uid:any;
   	adhar = {userid :'',adhar_no: '',full_name: '',type:'1',day:'',year:'',month:'',image_file:''};

	dd = [];
	mm = [];
	yy = [];
	  

   constructor(
   	            public navCtrl: NavController,
			   	private myservice: Myservice, 
			   	public storage: Storage, 
			   	public viewCtrl: ViewController,
			   	private toastCtrl: ToastController, 
			   	public navParams: NavParams,
			   	public firebaseService:FirebaseService,
			    public afAuth: AngularFireAuth
     ) {
   	this.adhar = this.navParams.get('upload_image');
   }


ionViewDidLoad() {
	//for days
	for (var i = 1; i <= 31; i++) {
				this.dd.push(i);
		}
	//for months
	this.mm.push({'value':'01', 'text':'Jan'});
	this.mm.push({'value':'02', 'text':'Feb'});
	this.mm.push({'value':'03', 'text':'Mar'});
	this.mm.push({'value':'04', 'text':'Apr'});
	this.mm.push({'value':'05', 'text':'May'});
	this.mm.push({'value':'06', 'text':'Jun'});
	this.mm.push({'value':'07', 'text':'Jul'});
	this.mm.push({'value':'08', 'text':'Aug'});
	this.mm.push({'value':'09', 'text':'Sep'});
	this.mm.push({'value':'10', 'text':'Oct'});
	this.mm.push({'value':'11', 'text':'Nov'});
	this.mm.push({'value':'12', 'text':'Dec'});
	//for years
	var currentYear = new Date().getFullYear();
	for (var j = currentYear-10; j > currentYear-70; j--) {
				this.yy.push(j);
		}

         var This = this;
        this.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
          This.uid = users.uid;
          this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.currentUser = snap.val();
          })
        }
    })


	}


   adharForm() {

	//this.myservice.show_loader();
	     var data = {  
             		"adhar_no":this.adhar.adhar_no,
             		"full_name":this.adhar.full_name,
             		"day":this.adhar.day,
        		    "month":this.adhar.month,
        			"year":this.adhar.year
                   
 				};
	this.rootdb.child('verification').child(this.uid).child("aadhar").update(data);

  }




  dismiss() {
   this.viewCtrl.dismiss();
 }

 goBack(){
    this.navCtrl.pop();
  }



}
