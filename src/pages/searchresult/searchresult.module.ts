import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Searchresult } from './searchresult';

@NgModule({
  declarations: [
    Searchresult,
  ],
  imports: [
    IonicPageModule.forChild(Searchresult),
  ],
  exports: [
    Searchresult
  ]
})
export class SearchresultModule {}
