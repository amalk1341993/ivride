import { Component } from '@angular/core';
import { IonicPage,NavController,ViewController,ToastController,NavParams,PopoverController,ModalController} from 'ionic-angular';
import {Myservice} from '../../providers/myservice';
import { Storage } from '@ionic/storage';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FirebaseService} from '../../providers/firebaseservice';

@IonicPage()
@Component({
  selector: 'page-searchresult',
  templateUrl: 'searchresult.html'
})
export class Searchresult {
  data:any;
  noride:any;
  data_check:any;
  duration:any;
  date:any;
  year:any;
  date_of_birth:any;
  age:any;
  kmcharge:any;
  user_data:any;
  ride_time:any;
  ampm:any;
  formatted_time:any;
  reached_time:any;
  formatted_reached_time:any;
  data1:any[]  =  [];
  rootdb:any;
  charge:any;
  users:any;
  ride:any;
  result:any;
  source:any;
  destination:any;
  rating:any;
  item_res:any;
  avg_hous:any;
  avg_min:any;
  avg_prices:any;
  req_date:any;
  constructor( 
                 public navCtrl: NavController,
                 private myservice: Myservice, 
                 public storage: Storage, 
                 public viewCtrl: ViewController,
                 private toastCtrl: ToastController, 
                 public navParams: NavParams,
                 public firebaseService:FirebaseService,
                 public afAuth: AngularFireAuth,
                 public popoverCtrl: PopoverController,
                 public modalCtrl: ModalController
              ) 
  {
      this.rootdb  = firebase.database().ref();
       this.item_res = new Array();
                  
        /*var This=this;
        This.rootdb  = firebase.database().ref(); 
        This.rootdb.child('settings').on('value',function(snap){
        This.charge = snap.val(); console.log(This.charge.kmcharge);
          This.kmcharge= This.charge.kmcharge;
        })*/   
              

              //this.data1 = navParams.get("firstPassed");
              //this.data_check = navParams.get("secondPassed");
              //this.duration = navParams.get("thirdpassed");

             


    


              this.data = navParams.get("data");
              var x = this.data.source.split(",");
              this.source = x[0];console.log(this.source);
              var y = this.data.destination.split(",");
              this.destination = y[0];
              this.req_date = this.data.departure_date;
              console.log(this.data);
              if(this.data){                
                var This = this;
                var sour_dest_date = this.data.source+"#"+this.data.destination+"#"+this.data.departure_date;
				        console.log(sour_dest_date);
				        this.rootdb.child('ride').orderByChild('sour_dest_date').equalTo(sour_dest_date).once('value',function(snap){
				              var res = snap.val();
                      console.log(res);
                      This.result = This.myservice.obj_array(res);
                      This.result.forEach((item,index) => {

                          if(This.data.rtrn_time<=item.departure_time){
                              if(This.data.seats<=item.no_of_seats){
                                var user_id = item.users_id;
                                This.rootdb.child('users').child(user_id).once('value',function(snap){
                                  var users = snap.val();
                                  This.date=new Date();
                                  This.year= This.date.getFullYear();
                                  if(users.dob){
                                    var dob = users.dob.split("-");
                                    users.age = This.year-dob[2]+" Y/O";
                                  } else {
                                    users.age = '';
                                  }
                                  item.users = users;
                                  item.reached_time = This.time_cal(item.departure_time,item.approximatetime);
                                  item.departure_time = This.set_hours(item.departure_time);
                                  item.reached_time = This.set_hours(item.reached_time);
                                  This.item_res.push(item);
                                  


                                })
                              }
                          }


                  	      //console.log(This.item_res);
                  	      
                  		    
                      


                  	})



                  	//This.result[index].departure_time =This.set_hours(item.departure_time);
                    //This.result[index].reached_time =This.set_hours(item.reached_time);


              /*This.rootdb.child('rating').orderByChild('owner').equalTo(user_id).once('value',function(snap){
                if(snap.val().exist()){

                    This.result[index].rating = snap.val();
                }
                
                    

                })*/





                  })
                  console.log(This.item_res);
                }



              }
    //}


  ionViewDidLoad() {
  }

  goBack(){
     this.navCtrl.pop();
   }

   time_cal(req_time,res_time){
     var req_array  = req_time.split(':');
     var res_array  = res_time.split(':');
     var hour = parseInt(req_array[0])+parseInt(res_array[0]);
     var min = parseInt(req_array[1])+parseInt(res_array[1]);
     if(min>=60){
       hour = hour+1;
       min = min-60;
     }

     if(hour>=24){
       hour = hour-24;
     }
     var time_min = min>10?min:"0"+min;
     return hour+":"+time_min+":00";
   }

   get_time(time,off){
     var time_array  = time.split(':');
     return time_array[off];
   }


  avg_duration(data_array){    
    var curr_time = "00:00";
    this.avg_hous = 0;
    this.avg_min = 0;
    var len = data_array.length;
    var hour = 0;
    var min_left = 0;
    data_array.forEach((item,index) => {
      
      var dates = item.approximatetime;
      var start = dates.split(":");
      var end = curr_time.split(":");
      var hour = parseInt(start[0])+parseInt(curr_time[0]);
      this.avg_min = parseInt(start[1])+parseInt(curr_time[1]);
        var ext = 0;
      if(this.avg_min>=60){
        var ext = this.avg_min/60;
        this.avg_min = (this.avg_min%60);

      } else {
        this.avg_min = this.avg_min;
      }
      hour = hour+ext;
      this.avg_hous =  hour;
      //this.avg_min = min_left;
      curr_time = hour+":"+this.avg_min;
      //console.log(this.avg_hous);
      //console.log(this.avg_min);
    });
      this.avg_hous = this.avg_hous/len;
      this.avg_hous = this.avg_hous>10?this.avg_hous:"0"+this.avg_hous;
      this.avg_min = this.avg_min>10?this.avg_min:"00";
    return this.avg_hous+":"+this.avg_min;



  }

  get_loc(loc){
    var y = loc.split(",");
    return y[0];
  }
  avg_price(data_array){
    var len = data_array.length;
    this.avg_prices = 0;
    data_array.forEach((item,index) => {
      this.avg_prices += item.price;
    })
    return this.avg_prices = Math.round(this.avg_prices/len);
  }

  check_date(req_date){
      this.date=new Date();
      var dd = this.date.getDate();
      var mm = this.date.getMonth()+1; //January is 0!
      var yyyy = this.date.getFullYear();

      if(dd<10) {
          dd = '0'+dd
      } 
      if(mm<10) {
          mm = '0'+mm
      } 
      var today = yyyy + '-' + mm + '-' + dd;
      if(today==req_date){
        return "Today";
      } else {
        return req_date;
      }
  }

   

   set_hours(time_val){
   	var time_array  = time_val.split(':');
   	var hour_val = 'Am';
   	if(time_array[0]>12){
   		hour_val = 'Pm';
   		time_array[0] = time_array[0] - 12;
   		time_array[0] = time_array[0]>=10?time_array[0]:"0"+time_array[0];
   	}
   	return time_array[0]+":"+time_array[1]+" "+hour_val;
   }

   filter() {
   let modal = this.modalCtrl.create('Filter');
   modal.present();
    modal.onDidDismiss(data => {
      if(data){
            console.log(data);



            var post_data={
               'destination':this.data.destination,
               'source':this.data.source,  
               'departure_date':data.travel_date,
               'rtrn_time':data.travel_time,
               'seats':data.seats
             }

     this.navCtrl.push('Searchresult',{'data': post_data});

      




  

                
                
                /*var This = this;
                this.rootdb  = firebase.database().ref(); 

        var sour_dest_date = this.data.source+"#"+this.data.destination+"#"+this.data.departure_date;
        console.log(sour_dest_date);
        this.rootdb.child('ride').orderByChild('sour_dest_date').equalTo(sour_dest_date).once('value',function(snap){
                        //this.rootdb.child('ride').orderByChild('destination').equalTo(this.data.destination).orderByChild('source').equalTo(this.data.source).once('value',function(snap){
                  var res = snap.val();
                  This.result = This.myservice.obj_array(res);

                  This.result.forEach((item,index) => {
                    var user_id = item.users_id;
                    This.rootdb.child('users').child(user_id).once('value',function(snap){
                      This.result[index].users = snap.val();

                     /* this.date=new Date();
                      this.year= this.date.getFullYear();
                      this.age = '';
                       var values = This.result[index].users.dob.split("-");
                      console.log(values);
                      this.age =this.year-values [0];console.log(this.age);
                      //console.log(this.dob);
                      this.data.age=this.age;console.log(this.data.age);*/


                   /* })

                    This.result[index].departure_time =This.set_hours(item.departure_time);
                    This.result[index].reached_time =This.set_hours(item.reached_time);
                  })
                  console.log(This.result);
                })*/



              








      //this.myservice.show_loader();
/*      this.myservice.load_post(data, "find_ride_filter").subscribe(response => {
      this.myservice.hide_loader();

      console.log(response);
      console.log(response.data);
          if(response.status == "success") {
             this.data =response.data;
               this.data.forEach((element,j) => {
                  if(element.date_of_birth){
                this.date_of_birth=element.date_of_birth;

                var values = this.date_of_birth.split("-");
                this.age =this.year-values [0];
                console.log(this.date_of_birth);
                this.data[j].age=this.age;
              
                  }
              });
          }
          
      })*/

      }

  });
   }

   alert() {
     var users = this.firebaseService.getCurrentUser();
     if(users){
        let modal = this.modalCtrl.create('Alert',{'data': this.data});
        modal.present();

     }else{
         let modal = this.modalCtrl.create('Loginpop');
      modal.present();
       modal.onDidDismiss(data => {

         var users = this.firebaseService.getCurrentUser();
             console.log(users)
             if(users!=null){
               let modal = this.modalCtrl.create('Alert',{'data': this.data});
               modal.present();
             }
   
       /*this.storage.get('userdata').then((userdata) => {
         this.user_data=userdata;
   
      console.log(this.user_data);
  })*/

 })
     }
  
   }


   searchmain(rideid) {
     console.log(rideid);
     var users = this.firebaseService.getCurrentUser();
     if(users){
        this.navCtrl.push('Searchmain',{'ride_id':rideid,'user_id':users.uid});
     }else{
        let modal = this.modalCtrl.create('Loginpop');
        modal.present();
        modal.onDidDismiss(data => {

         var users = this.firebaseService.getCurrentUser();
             console.log(users)
             if(users!=null){
               this.navCtrl.push('Searchmain',{'ride_id':rideid,'user_id':users.uid});
             }
       })
     }
         //console.log(user_id);
   
   }
}
