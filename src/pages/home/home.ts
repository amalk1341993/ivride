import { Component,ViewChild } from '@angular/core';
import { IonicPage,Nav,NavController,ModalController,ViewController,Events,NavParams,ToastController,Platform,PopoverController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FirebaseService} from '../../providers/firebaseservice';
import { Storage } from '@ionic/storage';
import {Myservice} from '../../providers/myservice';
import { InAppBrowser,InAppBrowserEvent} from '@ionic-native/in-app-browser';
import { AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home {
tab:any;
 currentUser: any;
  rootdb:any;
  uid:any;
  constructor(public navCtrl: NavController,
	 			 public modalCtrl: ModalController,
	 			 private toastCtrl: ToastController,
	 			 private myservice: Myservice,
	 			 public storage: Storage,
	 			 private formBuilder: FormBuilder,
	 			 public firebaseService:FirebaseService,
	 			 private iab: InAppBrowser,
	 			 public viewCtrl: ViewController,
	 			 public events: Events,
	 			 public navParams: NavParams,
	 			 private auth: AngularFireAuth,
		         public popoverCtrl: PopoverController
	 			 ) {
	  	
  }

  ionViewDidLoad() {
    this.tab = "active";
             var This = this;
        This.rootdb  = firebase.database().ref();
        const unsubscribe = firebase.auth().onAuthStateChanged( user => {
        if (user) {
          var users = this.firebaseService.getCurrentUser();
          This.uid = users.uid;
          this.rootdb.child('users').child(users.uid).on('value',function(snap){
            This.currentUser = snap.val();
          })
        }
    })
  }

  presentPopover(myEvent) {
let popover = this.popoverCtrl.create('Popovermore');
popover.present({
ev: myEvent
});
}

  offerride() {
    //this.myservice.hide_loader();
  this.navCtrl.push('Offerride');
}

findride() {
this.navCtrl.push('Findride');
}

profile() {
this.navCtrl.push('Profile');
}

tab_swap(type) {
    this.tab = type;
   }

   message() {
   this.navCtrl.push('Message');
   }

}
