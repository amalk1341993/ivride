import { BrowserModule } from '@angular/platform-browser';
import { HttpModule  } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {FirebaseService} from '../providers/firebaseservice';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { IonicStorageModule } from '@ionic/storage';
import * as firebase from 'firebase';
import { Myservice } from '../providers/myservice';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
import { OneSignal } from '@ionic-native/onesignal';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
//import { Google-distance-matrix } from 'google-distance-matrix';


export const firebaseConfig = {
    apiKey: "AIzaSyDlJjRztwYDct5fkaOW3gG9G5jNUacVNIo",
    authDomain: "joyride-2fa54.firebaseapp.com",
    databaseURL: "https://joyride-2fa54.firebaseio.com",
    projectId: "joyride-2fa54",
    storageBucket: "joyride-2fa54.appspot.com",
    messagingSenderId: "1087358974405"
  };

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
  HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
     AngularFireModule.initializeApp(firebaseConfig),
IonicStorageModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireAuthModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
  Myservice,
  FirebaseService,
    Geolocation,
    CallNumber,
    LocationAccuracy,
	//Google-distance-matrix,
    StatusBar,
     OneSignal,
    SplashScreen,
     FileTransfer,
     ImagePicker,
     Camera,
      InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}