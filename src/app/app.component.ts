import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,Events,MenuController ,ModalController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {FirebaseService} from '../providers/firebaseservice';
import { AngularFireAuth,AngularFireAuthProvider} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Storage } from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
  
  declare var cordova: any;
@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'Landing';
  currentUser: any;
  email:any;
  rootdb:any;
 newdata:any
   user_id:any;
   toast:any;
   push:any;
   userdata:any;
   location:any;
   lattitude:any;
   longitude:any;
   position:any;
  constructor(  public platform: Platform,
    public modalCtrl: ModalController,
                public statusBar: StatusBar, 
                public splashScreen: SplashScreen,
			          public firebaseService:FirebaseService,
			          public auth: AngularFireAuth,  
                public events: Events,
                public menuCtrl: MenuController,
                public storage: Storage,
                public oneSignal:OneSignal,
                public geolocation: Geolocation,
                public locationAccuracy: LocationAccuracy
                ) 
              {
/*
cordova.plugins.diagnostic.requestLocationAuthorization(function(status){

    //console.log("ABCDEEDEDDEEDE : "+status);
    switch(status){
        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
            console.log("Permission not requested");
            break;
        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
            console.log("Permission granted");
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED:
            console.log("Permission denied");
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
            console.log("Permission permanently denied");
            break;
        default : 
            console.log("EROORRORORORORORORO");
    }
}, function(error){
    console.error(error);
});
                  /*this.locationAccuracy.canRequest().then((canRequest: boolean) => {

  if(canRequest) {
    // the accuracy option will be ignored by iOS
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => console.log('Request successful'),
      error => console.log('Error requesting location permissions', error)
    );
  }
});*/
/*                
cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
      // alert("Location is " + (enabled ? "enabled" : "disabled"));
      if(!enabled) {cordova.plugins.diagnostic.switchToLocationSettings();}
})
                setInterval(() => {
                  this.get_crnt_location(); 
                }, 5000);

this.oneSignal.startInit('45b5a09b-9d0e-476d-a575-fa9f3088bb9c');
this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
this.oneSignal.handleNotificationReceived().subscribe(() => {

//  do something when notification is received
});
this.oneSignal.handleNotificationOpened().subscribe((getdata) => {
  // do something when a notification is opened
  // console.log(JSON.stringify((getdata)));
   this.newdata=getdata.notification.payload.additionalData;
   console.log(this.newdata);

    var users = this.firebaseService.getCurrentUser();
    this.userdata=users;
    this.user_id = users.uid;

     


  console.log("yss",this.userdata);
  if(this.userdata){
     console.log('data',this.newdata);
    console.log("yss",this.userdata);
    console.log("yss",this.userdata.id)
    if(this.newdata.push_type=='booked'){
      this.push='true';
        this.nav.setRoot('Message',{'data':this.newdata.result,'push':this.push});
    }else if(this.newdata.push_type=='ride_alert'){
         console.log(this.user_id);
        this.nav.setRoot('Ridepush',{'ride_id':this.newdata.ride_id,'user_id':this.userdata.id});
    }else if(this.newdata.push_type=='rating_alert'){
      this.nav.setRoot('Profile');
    }else if(this.newdata.push_type=='ride_start'){
      this.nav.setRoot('Yourride');
    }else if(this.newdata.push_type=='ride_approve_alert'){
      this.nav.setRoot('Yourride');
    }
  }else{
    console.log("no",this.userdata)
           let modal = this.modalCtrl.create('Loginpop');
      modal.present();
       modal.onDidDismiss(data => {
        if(data){
       this.storage.get('userdata').then((userdata) => {
        // setTimeout(() =>{
         this.userdata=userdata;
           console.log("no",this.userdata.id)
           this.user_id =  this.userdata.id
           console.log(this.user_id);
          // },1000)
          console.log(this.newdata);
      if(this.newdata.push_type=='booked'){
                //  alert('booked');
          this.push='true';
          this.nav.setRoot('Message',{'data':this.newdata.result,'push':this.push});
      }
      else if(this.newdata.push_type=='ride_alert'){
           this.nav.setRoot('Ridepush',{'ride_id':this.newdata.ride_id,'user_id':this.user_id });
      }else if(this.newdata.push_type=='ride_approve_alert'){
        this.nav.setRoot('Yourride');

      }
      console.log(this.userdata);
    })
   }
 })
  }
});
this.oneSignal.endInit();

*/







                this.storage.get('welcome_status').then((val) => {
                  console.log(val);
                  if(val!=1){
                    this.rootPage = 'Landing';
                    this.storage.set('welcome_status', '1');
                  } else {
                    this.rootPage = 'Home';
                  }
                });



                var This = this;
                this.rootdb  = firebase.database().ref();
                const unsubscribe = firebase.auth().onAuthStateChanged( user => {
                 if (!user) {
                    //this.rootPage = 'Home';
                  } else { 
                    this.rootPage = 'Home';
                    var users = this.firebaseService.getCurrentUser();
                    this.rootdb.child('users').child(users.uid).on('value',function(snap){
                      This.currentUser = snap.val();
                    })
                  }
                });
                 this.initializeApp();


       platform.ready().then(() => {




      // get position
      geolocation.getCurrentPosition().then(pos => {
        console.log(`lat: ${pos.coords.latitude}, lon: ${pos.coords.longitude}`)
      });


      // watch position
      const watch = geolocation.watchPosition().subscribe(pos => {
        console.log(`lat: ${pos.coords.latitude}, lon: ${pos.coords.longitude}`)
        this.position = pos;
      });

      // to stop watching
      watch.unsubscribe();
    });


              }
  initializeApp() {





    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  
  editprofile() {
   this.nav.setRoot('Editprofile');
 }
  home() {
   this.nav.setRoot('Home');
 }
   signup() {
   this.nav.setRoot('Signup');
 }
 offerride() {
  this.nav.setRoot('Offerride');
}

yourride(){
  this.nav.setRoot('Yourride');
}

offeredride(){
  this.nav.setRoot('Offered');
}

findride() {
 this.nav.setRoot('Findride');
}

  logout() {
    var This = this;
  	firebase.auth().signOut().then(function() {
  	   console.log("Logged out!");
  	   This.nav.setRoot('Login');
       //This.menuCtrl.enable(false);
  	   //this.nav.push("Login");
  	}, function(error) {
  	   console.log(error.code);
  	   console.log(error.message);
  	});
  }
  
  login() {
    this.nav.setRoot('Login');
  }


  get_crnt_location(){
        //  platform.ready().then(() => {
      var This=this;
      This.geolocation.getCurrentPosition().then((location) => {
      console.log(location);
      This.location=location;
      console.log(this.location.coords.latitude);
      console.log(this.location.coords.longitude);
      This.lattitude=this.location.coords.latitude;
      This.longitude=this.location.coords.longitude
      var data={'latitude':this.location.coords.latitude,
                'longitude':this.location.coords.longitude
                }
      console.log(data);
      var users = This.firebaseService.getCurrentUser();
      if(users){
        console.log("updated");
        This.rootdb  = firebase.database().ref();
         var res = This.rootdb.child('users').child(users.uid).update(data);        
      }
      }, err => {
        console.log('Error message : '+ err.message);
      })
       //this.location=This.location;
  }

}
